sap.ui.define(
    function() {
        "use strict";

        var library = sap.ui.getCore().initLibrary({
            name: "de.jkeller.reuse.validator",
            version: "0.1-SNAPSHOT",
            dependencies: [
                "sap.ui.core"
            ],
            interfaces: [
                "de.jkeller.reuse.validator.SimpleValidator"
            ]
        });	
    
        return library; 
    }
);