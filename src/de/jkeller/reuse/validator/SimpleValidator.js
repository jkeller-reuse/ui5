sap.ui.define([
    "sap/ui/base/ManagedObject",
    "sap/ui/model/resource/ResourceModel",
    "sap/ui/core/Core"
],

    function (ManagedObject, ResourceModel) {
        "use strict";

        return ManagedObject.extend("de.jkeller.reuse.validator.SimpleValidator", {
            /**
             * Create a new SimpleValidator.
             * @public
             * @param {object} i18nModel optional. If provided it will be used as i18n model. Otherwise library messagebundle will used as default.
             */
            constructor: function (i18nModel) {
                ManagedObject.apply(this, []);
                this.setModel(i18nModel? i18nModel: sap.ui.getCore().getLibraryResourceBundle("de.jkeller.reuse.validator"), "i18n");
            },

            /**
             * Validate the field group identified by provided grupId.
             * @public
             * @param {string} groupId identifying the field group that shall be validated. All included inputs will be validated.
             * @returns true if all inputs in this field group contain valid inputs, false if any does not contain a valid input.
             */
            validateFieldGroup: function(groupId) {
                return this.validateControls(sap.ui.getCore().byFieldGroupId(groupId));
            },

            /**
             * Validate the field group identified by provided grupId.
             * @public
             * @param {string} groupId identifying the field group that shall be validated. All included inputs will be validated.
             * @returns {typeof Array} array containing invalid controls (if array is empty all controls have valid inputs).
             */
            validateFieldGroupReceivingInvalid: function(groupId) {
                return this.validateControlsReceivingInvalid(sap.ui.getCore().byFieldGroupId(groupId));
            },

            /**
             * Validate the provided controls.
             * @public
             * @param {Array} controls validate all controls contained in this array.
             * @returns true if all inputs in the controls array are valid, false if any does not contain a valid input.
             */
            validateControls: function(controls) {
                const invalidControls = this.validateControlsReceivingInvalid(controls);
                return invalidControls && invalidControls.length === 0;
            },

            /**
             * Validate the provided controls.
             * @public
             * @param {Array} controls validate all controls contained in this array.
             * @returns {typeof Array} array containing invalid controls (if array is empty all controls have valid inputs).
             */
            validateControlsReceivingInvalid: function(controls) {
                let invalidControls = [];
                if(controls) {
                    for(var i = 0; i < controls.length; ++i) {
                        this._checkControl(controls[i], false, invalidControls);
                    }
                }

                return invalidControls;
            },

            /**
             * Validate the provided control.
             * @public
             * @param {object} control to be validated
             * @returns true if the provided control contains valid input, false if input is invalid.
             */
            validate: function(control) {
                const invalidControls = this.validateReceivingInvalid(control);
                return invalidControls && invalidControls.length === 0;
            },

            /**
             * Validate the provided control.
             * @public
             * @param {object} control to be validated
             * @returns {typeof Array} array containing invalid controls (if array is empty all controls have valid inputs).
             */
            validateReceivingInvalid: function(control) {
                let object = typeof control === "string"? sap.ui.getCore().byId(control): control;
                let invalidControls = []

                if(object && object.getVisible()) {
                    this._checkControl(object, true, invalidControls);
                }

                return invalidControls;
            },

            /**
             * Check the provided control and its contained child controls if flag is set to true.
             * @private
             * @param {object} control 
             * @param {boolean} checkChildren default true. If set to true child controls will be checked, if set to false no children will 
             *      be checked.
             * @param {Array} invalidControls list of controls containing validation errors.
             * @returns true if the input in this control is valid, false if it is invalid.
             */
            _checkControl: function(control, checkChildren = true, invalidControls = []) {
                const VALIDATE_PROPERTIES = ["value", "selectedKey", "text"];
                let isValid = true;
                let isValidated = false;

                if(!control) {
                    return;
                }

                if(control.getVisible && control.getVisible()) {
                    if(control instanceof sap.m.InputBase || control instanceof sap.m.Select || control instanceof sap.m.StepInput) {
                        for(var i = 0; i < VALIDATE_PROPERTIES.length; ++i) {
                            let validateProperty = VALIDATE_PROPERTIES[i];
                            let binding = control.getBinding? control.getBinding(validateProperty): null;

                            if(this._hasControlProperty(control, validateProperty) 
                               && (binding || control.getRequired && control.getRequired())) {
                                let isValidInput = this._validateControl(control, validateProperty);
                                isValid = isValid? isValidInput: isValid;

                                if(!isValid && invalidControls) {
                                        invalidControls.push(control);
                                }

                                this._attachChangeIfRequired(control);

                                isValidated = true;
                                break;
                            } 
                        }
                    }

                    if(!isValidated && checkChildren) {
                        isValid = isValid? this._checkChildControls(control, invalidControls): isValid;
                    }
                }

                return isValid;
            },

            /**
             * Check the child controls of the provided control. This includes the aggregation information 'content'.
             * @private
             * @param {object} control to be validated
             * @param {Array} invalidControls list of controls containing validation errors.
             * @returns true if the input of the control is valid, false if it is invalid.
             */
            _checkChildControls(control, invalidControls) {
                let isValid = true;
                let contentElements = control.getAggregation("content");
                contentElements = contentElements? contentElements: control.findElements();

                if(contentElements ) {
                    if(contentElements instanceof Array) {
                        for(var i = 0; i < contentElements.length; ++i) {
                            let isValidInput = this._checkControl(contentElements[i], true, invalidControls);
                            isValid = isValid? isValidInput: isValid;
                        }
                    }
                }

                return isValid;
            },

            /**
             * Add a change listener to the control if this is required, i.e., a sap.m.Select / sap.m.DatePicker.
             * This is necessary as otherwise the error message and value state of a Select or DatePicker may not be removed.
             * @private
             * @param {object} control 
             */
            _attachChangeIfRequired(control) {
                if(control.getValueState() === sap.ui.core.ValueState.Error 
                    && control.getValueStateText() === this.getModel("i18n").getText("requiredInputEmpty")) {
                    switch(control.constructor) {
                        case sap.m.Select:
                            this._attachSelectChange(control);
                            break;
                        case sap.m.DatePicker:
                            this._attachInputBaseChange(control);
                            break;
                    }
                }
            },

            /**
             * Validate the provided control for the provided property.
             * @private
             * @param {object} control to be validated
             * @param {string} validateProperty property of the control that shall be validated
             * @returns true if the control contains valid input, false if it contains invalid input.
             */
            _validateControl: function(control, validateProperty) {
                let isValid = true;

                if(control.getVisible() && this._hasControlProperty(control, validateProperty)) {
                    let value = control.getProperty(validateProperty);
                    let initialValueState = control.getValueState();
                    let currentFocusedObjectId = sap.ui.getCore().getCurrentFocusedControlId();
                    // try to trigger built in validation, e.g., StepInput as focus out event doesn't seem to work...
                    if(currentFocusedObjectId === control.getId()) {
                        control.getParent().focus();
                    } else {
                        control.focus();
                    }
                    
                    let previousFocusedControl = sap.ui.getCore().byId(currentFocusedObjectId);
                    if(previousFocusedControl) {
                        previousFocusedControl.focus();
                    }

                    isValid = control.getValueState() === sap.ui.core.ValueState.Error? false: true;

                    let binding = control.getBinding? control.getBinding(validateProperty): null;
                    if(control.getValueState() === initialValueState && initialValueState !== sap.ui.core.ValueState.Error 
                       && binding && binding.getType() && (control.getRequired() || value)) {
                        try {
                            binding.getType().validateValue(value);
                            control.setValueState(sap.ui.core.ValueState.None);
                            control.setValueStateText("");
                            isValid = true;
                        } catch(e) {
                            control.setValueState(sap.ui.core.ValueState.Error);
                            control.setValueStateText(e.message);
                            isValid = false;
                        }
                    }

                    if(control.getRequired() && !value && value !== 0) {
                        control.setValueState(sap.ui.core.ValueState.Error);
                        control.setValueStateText(this.getModel("i18n").getText("requiredInputEmpty"));
                        isValid = false;
                    } else if(control.getRequired() && value 
                             && control.getValueStateText() === this.getModel("i18n").getText("requiredInputEmpty")) {
                        control.setValueState(sap.ui.core.ValueState.None);
                        control.setValueStateText("");
                        isValid = true;
                    }
                } else {
                    isValid = false;
                }

                return isValid;
            },

            /**
             * Attach a change listener to a select. This listener will be removed if it was already attached.
             * @private
             * @param {object} control the change listener will be attached to
             */
            _attachSelectChange: function(control) {
                if(control instanceof sap.m.Select) {
                    control.detachChange(this._selectChange, this);
                    control.attachChange(this._selectChange, this);
                }
            },

            /**
             * Change listener that will remove error state and message if the value of the select was changed
             * @private
             * @param {object} event 
             */
            _selectChange: function(event) {
                let select = event.getSource();
                if(select.getSelectedItemId && select.getSelectedItemId()) {
                    select.setValueState(sap.ui.core.ValueState.None);
                    select.setValueStateText("");
                    select.detachChange(this._selectChange, this);
                }
            },

            /**
             * @private
             * @param {object} control the change listener will be attached to
             */
            _attachInputBaseChange: function(control) {
                if(control instanceof sap.m.InputBase) {
                    control.detachChange(this._inputBaseChange, this);
                    control.attachChange(this._inputBaseChange, this);
                }
            },

            /**
             * Change listener that will remove error state and message if the value of the input was changed to a valid one.
             * @private
             * @param {object} event 
             */
            _inputBaseChange: function(event) {
                let input = event.getSource();
                if(this._validateControl(input, "value")) {
                    input.setValueState(sap.ui.core.ValueState.None);
                    input.setValueStateText("");
                    input.detachChange(this._inputBaseChange, this);
                }
            },

            /**
             * Check if provided control has property.
             * @param {object} control
             * @param {string} propertyName to check for existence in control.
             * @returns true if the control contains the property, false otherwise.
             */
            _hasControlProperty: function(control, propertyName) {
                return control && control.getMetadata && control.getMetadata() && control.getMetadata().getAllProperties 
                    && control.getMetadata().getAllProperties().hasOwnProperty(propertyName);
            }
        });
    }
);