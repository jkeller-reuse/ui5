sap.ui.define([
    "sap/ui/model/json/JSONModel"
],
    /**
     * Class providing functionality to interact via JSONs. It provides functionalities for receiving and sending JSON.
     * @param {typeof sap.ui.model.json.JSONModel} JSONModel
     */
    function (JSONModel) {
        "use strict";

        const CONTENT_JSON = "application/json; charset=utf-8";
        const METHOD = {
            POST: "POST",
            GET: "GET",
            PUT: "PUT",
            DELETE: "DELETE"
        }

        return JSONModel.extend("de.jkeller.reuse.model.JSONModelExtended", {
            /**
             * Create a new JSONModelExtended instance. If data was provided and it is an object it will be added to the stored
             * JSON object. If provided data is a string it will be treated as URL to lad the JSON, if flag isBaseUrl is set
             * the provided data string will be treated as base URL and therefore not loaded.
             * @public
             * @param {object | string} data is an object it will be added to the stored JSON object. If it is a string it will 
             *      be treated as URL and the JSON of this URL will be loaded, if flag isBaseUrl is set the provided data string 
             *      will be treated as base URL and therefore not loaded.
             * @param {boolean} isBaseUrl identifier whether provided data is a base URL. If set to true JSON data of the provided 
             *      data URL string will not be loaded automatically, but URL is treated and stored as base URL.
             * @param {boolean} observe 
             */
            constructor: function (data, isBaseUrl = true, observe) {
                var doNotSetData = typeof data === "string" && isBaseUrl;
                this._baseURL = typeof data === "string" && isBaseUrl? data: "";
                JSONModel.apply(this, [doNotSetData? {}: data, observe]);
            },

            /**
             * Load the data of the provided URL. If no url is provided it will load data from the stored base URL (if it was 
             * provided during creation). If a base URL is stored the provided url will be treated as relative to the base URL.
             * @public
             * @param {string} url If no url is provide it will load data from the stored base URL. If a base URL is stored the provided url
             *      will be treated as relative to the base URL. 
             * @param {object} settings specified for the load call. async (boolean): true for async call, false for sync call. 
             *      merge (boolean): true if the received JSON object shall be merged into the existing stored one, false it shall replace the 
             *      stored JSON object. urlParameters (object): JSON object of URL parameter that shall be used in the call made.
             */
            load: function(url, settings = {}) {
                const OPTIONS = settings? settings: {};
                this.loadData(this._getURLString(url, OPTIONS.urlParameters), "", 
                              OPTIONS.hasOwnProperty("async")? OPTIONS.async: true, 
                              METHOD.GET, OPTIONS.hasOwnProperty("merge")? OPTIONS.merge: false);
            },

            /**
             * Make a specified call to a provided URL.
             * @public
             * @param {string} url If no url is provide it will load data from the stored base URL. If a base URL is stored the provided url
             *      will be treated as relative to the base URL.
             * @param {string} method default 'GET'. @example GET, POST, PUT, DELETE
             * @param {object} settings specified for the load call. async (boolean): true for async call, false for sync call. 
             *      merge (boolean): true if the received JSON object shall be merged into the existing stored one, false it shall replace the 
             *      stored JSON object. urlParameters (object): JSON object of URL parameter that shall be used in the call made.
             *      success (function): callback function if call was successful.
             *      error (function): callback function if call fails.
             * @param {string | object} data optional. If provided this data is sent by the call in the body of the request. If the provided data is 
             *      a string it will be parsed into a JSON object.
             * @returns the loaded JSON data for a sync call.
             */
            call: function(url, method = METHOD.GET, settings = {}, data) {
                const OPTIONS = settings? settings: {};
                let that = this;

                let async = OPTIONS.hasOwnProperty("async")? OPTIONS.async: true;
                let mergeData = OPTIONS.hasOwnProperty("merge")? OPTIONS.merge: false;
                let sendData = typeof data === "object"? JSON.stringify(data): data;
                let syncResult;

                $.ajax({
                    url: this._getURLString(url, OPTIONS.urlParameters),
                    method: method,
                    contentType: CONTENT_JSON,
                    dataType: "json",
                    async: async,
                    data: sendData,
                    success: function(responseData, status, xhttp) {
                        if(mergeData || OPTIONS.merging) {
                            let storedData = JSON.parse(that.getJSON());

                            if(storedData instanceof Array) {
                                storedData.unshift(responseData);
                            } else {
                                storedData = Object.assign(JSON.parse(that.getJSON()), responseData);
                            }
                            
                            that.setData(storedData);
                        }

                        if(!async) {
                            syncResult = responseData;
                        }

                        if(OPTIONS.success && typeof OPTIONS.success === "function") {
                            OPTIONS.success(responseData, status, xhttp);
                        }
                    },
                    error: function(xhttp, status, errorThrown) {
                        if(!async) {
                            syncResult = errorThrown;
                        }

                        if(OPTIONS.error && typeof OPTIONS.error === "function") {
                            OPTIONS.error(xhttp, status, errorThrown);
                        }
                    }
                });

                if(!async) {
                    return syncResult;
                }
            },

            /**
             * Make a POST call to the provided URL. The request body will either contain the provided data JSON object, if this was not 
             * provided the currently stored JSON by thi model will be used.
             * @param {string} url If no url is provide it will load data from the stored base URL. If a base URL is stored the provided url
             *      will be treated as relative to the base URL.
             * @param {object} settings specified for the load call. async (boolean): true for async call, false for sync call. 
             *      merge (boolean): true if the received JSON object shall be merged into the existing stored one, false it shall replace the 
             *      stored JSON object. urlParameters (object): JSON object of URL parameter that shall be used in the call made.
             *      success (function): callback function if call was successful.
             *      error (function): callback function if call fails.
             * @param {string | object} data optional. If provided this data is sent by the call in the body of the request. If the provided data is 
             *      a string it will be parsed into a JSON object. If no data was provided the currently stored data in this model will be sent.
             * @returns the loaded JSON data for a sync call.
             */
            create: function(url, settings = {}, data) {
                const JSON_DATA = data? data: this.getJSON();
                
                return this.call(url, METHOD.POST, settings, JSON_DATA);
            },

            /**
             * Receive a search parameter string of the provided urlParameter object.
             * @private
             * @param {object} urlParameters 
             * @returns 
             */
            _getURLParameterString: function(urlParameters) {
                if(urlParameters && typeof urlParameters === 'object') {
                    let searchParams = new URLSearchParams();
                    for(var key in urlParameters) {
                        searchParams.append(key, urlParameters[key]);
                    }

                    return searchParams.toString();
                }

                return "";
            },

            /**
             * Receive the URL that shall be used. This will use the base URL if one is stored, then provided url parameter will 
             * be treated as relative. Also provide URL parameters are considered and added to the URL string.
             * @private
             * @param {string} url 
             * @param {object} urlParameters 
             * @returns string of a URL that shall be used.
             */
            _getURLString: function(url, urlParameters) {
                const SLASH = "/";
                let urlString = SLASH;

                if(url && this._baseURL) {
                    let urlParts = [
                        this._baseURL.match(/^\w+\:\/\//) || this._baseURL.startsWith(SLASH)? this._baseURL: SLASH + this._baseURL,
                        this._baseURL.endsWith(SLASH) && url.startsWith(SLASH)? url.replace(/^\//, ""):
                            !this._baseURL.endsWith(SLASH) && !url.startsWith(SLASH)? SLASH + url: url
                    ];

                    urlString = urlParts.join("");
                } else if(!url && this._baseURL) {
                    urlString = this._baseURL;
                } else {
                    urlString = url;
                }

                let queryString = this._getURLParameterString(urlParameters);
                queryString = queryString? "?" + queryString: queryString;

                return urlString + queryString;
            },

            setBaseURL: function(baseURL) {
                this._baseURL = baseURL;
            },

            getBaseURL: function() {
                return this._baseURL;
            }
        });
    }
);
