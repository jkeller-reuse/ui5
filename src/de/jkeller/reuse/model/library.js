sap.ui.define(
    function() {
        "use strict";

        var library = sap.ui.getCore().initLibrary({
            name: "de.jkeller.reuse.model",
            version: "0.1-SNAPSHOT",
            dependencies: [
                "sap.ui.core"
            ],
            interfaces: [
                "de.jkeller.reuse.model.JSONModelExtended"
            ]
        });	
    
        return library; 
    }
);