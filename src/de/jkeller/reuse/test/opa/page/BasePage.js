sap.ui.define([
    "sap/ui/base/Object",
    "sap/ui/test/Opa5",
    "sap/ui/test/matchers/PropertyStrictEquals",
    "sap/ui/test/matchers/BindingPath",
    "sap/ui/test/matchers/Properties",
    "sap/ui/test/matchers/Ancestor",
    "sap/ui/test/actions/Press",
    "sap/ui/test/actions/EnterText",
    "sap/ui/test/matchers/LabelFor",
    "de/jkeller/reuse/test/actions/SlideHandle"
], function (Object, Opa5, PropertyStrictEquals, BindingPath, Properties, Ancestor, Press, EnterText, LabelFor, SlideHandle) {
    "use strict";

    let opa5 = new Opa5();

    return Object.extend("de.jkeller.reuse.test.opa.page.BasePage", {
        constructor: function(viewName) {
            this._base.viewName = viewName;
            this.init();
        },

        _base: {
            getViewName: function() {
                return this.viewName;
            },
    
            setViewName: function(viewName) {
                this.viewName = viewName;
            },

            getOpa5: function() {
                return this instanceof sap.ui.test.Opa5? this: opa5;
            }
        },

        init: function() {
            let that = this;

            this.actions = {
                ... that._base,
                ... that.actions
            }
            this.assertions = {
                ... that._base,
                ... that.assertions
            }

            return this;
        },

        actions: {
            iClickButtonWithLabel: function(label) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "text",
                            value: label
                        })
                    ],
                    actions: [new Press()],
                    errorMessage: `Could not find button with label '${label}'.`
                });
            },

            iClickButtonWithId: function(buttonId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: buttonId,
                    visible: true,
                    controlType: "sap.m.Button",
                    actions: [new Press()],
                    errorMessage: `Could not find button with id '${buttonId}'.`
                });
            },

            iClickOverflowToolbarButtonWithId: function(buttonId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: buttonId,
                    visible: true,
                    controlType: "sap.m.OverflowToolbarButton",
                    actions: [new Press()],
                    errorMessage: `Could not find overflow toolbar button with id '${buttonId}'.`
                });
            },

            iPressTheBackButton: function (pageId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: pageId,
                    success: function (page) {
                        page.$("navButton").trigger("tap");
                    },
                    errorMessage: "Did not find the nav button on object page"
                });
            },

            iSelectItemFromSelectWithId: function(selectId, itemKey) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: selectId,
                    controlType: "sap.m.Select",
                    visible: true,
                    success: function(select) {
                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            matchers: [
                                new Ancestor(select),
                                new Properties({key: itemKey})
                            ],
                            actions: [new Press()],
                            errorMessage: `Could not find element with key: '${itemKey}' in select with id '${selectId}'.`
                        });
                    },
                    errorMessage: `Could not find select with id '${selectId}'.`
                });
            },

            iSelectItemFromSelectWithBinding: function(bindingSettings, itemKey) {
                let OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    controlType: "sap.m.Select",
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS)
                    ],
                    actions: [new Press()],
                    success: function(selects) {
                        let select = selects[0];

                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            matchers: [
                                new Ancestor(select),
                                new Properties({key: itemKey})
                            ],
                            actions: [new Press()],
                            errorMessage: `Could not find element with key: '${itemKey}' in select with binding '${JSON.stringify(OPTIONS)}'.`
                        });
                    },
                    errorMessage: `Could not find select with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iClickDialogButtonWithLabel: function(label) {
                return this.getOpa5().waitFor({
                    controlType: "sap.m.Dialog",
                    visible: true,
                    success: function(dialogs) {
                        let dialog = dialogs[0];

                        this.getOpa5().waitFor({
                            controlType: "sap.m.Button",
                            matchers: [
                                new Ancestor(dialog), 
                                new PropertyStrictEquals({
                                    name: "text",
                                    value: label
                                })
                            ],
                            actions: [new Press()],
                            errorMessage: `Button with label '${label}' not found in dialog.`
                        });
                    },
                    errorMessage: "No Dialog found."
                });
            },

            iEnterTextInInputWithId: function(inputId, text) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: inputId,
                    visible: true,
                    actions: [new EnterText({text: text})],
                    errorMessage: `Input with id '${inputId}' not found.`
                });
            },

            iEnterTextInInputWithBinding: function(bindingSettings, text) {
                const OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS)
                    ],
                    actions: [new EnterText({text: text})],
                    errorMessage: `Could not find input with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iSlideHandleOfElementWithId: function(elementId, slideOffset) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    id: elementId,
                    actions: [new SlideHandle({xValueOffset: slideOffset})],
                    errorMessage: `Element with id '${elementId}' not found.`
                });
            },

            iSlideHandleOfElementWithLabel: function(label, slideOffset) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    matchers: [
                        new LabelFor({
                            text: label
                        })
                    ],
                    actions: [new SlideHandle({xValueOffset: slideOffset})],
                    errorMessage: `Element with label '${label}' not found.`
                });
            },

            iClickAvatarWithId: function(avatarId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: avatarId,
                    controlType: "sap.m.Avatar",
                    actions: [new Press()],
                    errorMessage: `Could not find avatar with id '${avatarId}'.`
                }); 
            },

            iClickStandardListItemWithId: function(listItemId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: listItemId,
                    controlType: "sap.m.StandardListItem",
                    visible: true,
                    actions: [new Press()],
                    errorMessage: `Could not find avatar with id '${listItemId}'.`
                }); 
            },

            iOpenValueHelpOfInputWithId: function(inputId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: inputId,
                    visible: true,
                    actions: [new Press({idSuffix: "vhi"})],
                    errorMessage: `Input with id '${inputId}' not found.`
                });
            },

            iOpenValueHelpOfInputWithBinding: function(bindingSettings) {
                const OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS)
                    ],
                    actions: [new Press({idSuffix: "vhi"})],
                    errorMessage: `Could not find input with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iPressItemOfTableWithId: function(tableId, itemIndex) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: tableId,
                    visible: true,
                    controlType: "sap.m.Table",
                    success: function(table) {
                        const items = table.getItems();
                        Opa5.assert.ok(items && items.length > itemIndex, `Table with id '${tableId} contains item with index ${itemIndex}.`);
                        const item = items[itemIndex];

                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            visible: true,
                            controlType: item.getMetadata().getName(),
                            matchers:[
                                new Ancestor(table),
                                new PropertyStrictEquals({
                                    name: "id",
                                    value: item.getId()
                                })
                            ],
                            actions: [new Press()],
                            errorMessage: `Item '${itemIndex}' of table with id '${tableId}' not pressed.`
                        });
                    },
                    errorMessage: `Could not find table with id '${tableId}'.`
                });
            },

            iClickUXAPHeaderTitle: function() {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.uxap.ObjectPageDynamicHeaderTitle",
                    actions: [new Press()],
                    errorMessage: "Could not press header title."
                });
            },

            iClickButtonWithIcon: function(icon) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "icon",
                            value: icon
                        })
                    ],
                    actions: [new Press()],
                    errorMessage: `Could not find button with icon '${icon}'.`
                });
            },

            iClosePopOver: function() {
                return this.getOpa5().waitFor({
                    visible: true,
                    controlType: "sap.m.Popover",
                    actions: function(popovers) {
                        const POPOVERS = popovers instanceof Array ? popovers : [popovers];
                        POPOVERS.forEach((popover) => {
                            popover.close();
                            Opa5.assert.ok(true, "Popover closed");
                        });
                    }
                });
            }
        },

        assertions: {
            iShouldSeeTheTable: function(tableId) {
                let opa = new Opa5();
                return opa.waitFor({
                    viewName: this.getViewName(),
                    id: tableId,
                    visible: true,
                    success: function() {
                        Opa5.assert.ok(true, `Table with id '${tableId} is shown.`);
                    },
                    errorMessage: `Could not find table with id '${tableId}'.`
                });
            },

            iTheTableContainsRecords: function(tableId, numRecords, allowCheckEmpty=false) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: tableId,
                    visible: true,
                    success: function(table) {
                        if(numRecords && allowCheckEmpty) {
                            Opa5.assert.equal(table.getItems().length, numRecords, `Table with id '${tableId} contains expected ${numRecords} records.`);
                        } else {
                            Opa5.assert.ok(table.getItems().length > 0, `Table with id '${tableId} contains records.`);
                        }
                    },
                    errorMessage: `Could not find table with id '${tableId}' or it does not contain expected number of records.`
                });
            },

            iShouldBeOnPage: function(pageId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: pageId,
                    controlType: "sap.m.Page",
                    visible: true,
                    success: function(page) {
                        Opa5.assert.ok(true, `Page with id '${pageId}' is shown.`);
                    },
                    errorMessage: `Could not find page with id '${pageId}'.`
                });
            },

            iShouldSeeButtonWithLabel: function(label, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "text",
                            value: label
                        })
                    ],
                    success: function(buttons) {
                        let button = buttons[0];
                        if(!buttonType) {
                            Opa5.assert.ok(true, `The button with label '${label}' is shown.`);
                        } else {
                            Opa5.assert.equal(button.getType(), buttonType, `The button with label '${label}' and type '${buttonType}' is shown.`);
                        }
                    },
                    errorMessage: `Could not find button with label '${label}'.`
                });
            },

            iShouldSeeButtonWithId: function(buttonId, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: buttonId,
                    visible: true,
                    controlType: "sap.m.Button",
                    success: function(button) {
                        if(!buttonType) {
                            Opa5.assert.ok(true, `The button with label '${label}' is shown.`);
                        } else {
                            Opa5.assert.equal(button.getType(), buttonType, `The button with label '${label}' and type '${buttonType}' is shown.`);
                        }
                    },
                    errorMessage: `Could not find button with id '${buttonId}'.`
                });
            },

            iShouldSeeOverflowToolbarButtonWithId: function(buttonId, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: buttonId,
                    visible: true,
                    controlType: "sap.m.OverflowToolbarButton",
                    success: function(button) {
                        if(!buttonType) {
                            Opa5.assert.ok(true, `The overflow toolbar button with id '${buttonId}' is shown.`);
                        } else {
                            Opa5.assert.equal(button.getType(), buttonType, `The overflow toolbar button with id '${buttonId}' and type '${buttonType}' is shown.`);
                        }
                    },
                    errorMessage: `Could not find overflow toolbar button with id '${buttonId}'.`
                });
            },

            iShouldNotSeeOverflowToolbarButtonWithId: function(buttonId, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: buttonId,
                    visible: false,
                    controlType: "sap.m.OverflowToolbarButton",
                    success: function(button) {
                        Opa5.assert.notOk(button.getVisible(), `The overflow toolbar button with id '${buttonId}' is hidden.`);
                    },
                    errorMessage: `The overflow toolbar button with id '${buttonId}' is visible.`
                });
            },

            iShouldNotSeeButtonWithLabel: function(label, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: false,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "text",
                            value: label
                        })
                    ],
                    success: function(buttons) {
                        let button = buttons[0];
                        if(!buttonType) {
                            Opa5.assert.notOk(button.getVisible(), `The button with label '${label}' is not shown.`)
                        } else {
                            Opa5.assert.notOk(button.getVisible(), `The button with label '${label}' is not shown.`)
                            Opa5.assert.equal(button.getType(), buttonType, `The button with label '${label}' and type '${buttonType}' is not shown.`)
                        }
                    },
                    errorMessage: `Could see button with label '${label}'.`
                });
            },

            iShouldNotSeeButtonWithId: function(buttonId, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: buttonId,
                    visible: true,
                    controlType: "sap.m.Button",
                    success: function(button) {
                        if(!buttonType) {
                            Opa5.assert.notOk(button.getVisible(), `The button with id '${buttonId}' is not shown.`)
                        } else {
                            Opa5.assert.notOk(button.getVisible(), `The button with id '${buttonId}' is not shown.`)
                            Opa5.assert.equal(button.getType(), buttonType, `The button with id '${buttonId}' and type '${buttonType}' is not shown.`)
                        }
                    },
                    errorMessage: `Could see button with id '${buttonId}'.`
                });
            },

            iShouldSeeElementWithBinding: function(settings, elementType) {
                const OPTIONS = settings? settings: {};
                const waitForObject = {
                    viewName: this.getViewName(),
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS)
                    ],
                    success: function(elements) {
                        let element = elements[0];

                        if(!elementType) {
                            Opa5.assert.ok(true, `The element with binding information '${JSON.stringify(OPTIONS)}' was found.`);
                        } else {
                            Opa5.assert.equal(element.getMetadata().getName(), elementType, `The element of type '${elementType}' with binding information '${JSON.stringify(OPTIONS)}'.`);
                        }
                    },
                    errorMessage: `Could not find element with binding information '${JSON.stringify(OPTIONS)}'.`
                };

                if(elementType) {
                    waitForObject.controlType = elementType;
                }

                return this.getOpa5().waitFor(waitForObject);
            },

            iShouldSeeDialogWithTitle: function(title, displayedText) {
                return this.getOpa5().waitFor({
                    controlType: "sap.m.Dialog",
                    visible: true,
                    matchers: [
                        new PropertyStrictEquals({
                            name: "title",
                            value: title
                        })
                    ],
                    success: function(dialogs) {
                        let dialog = dialogs[0];

                        if(!displayedText) {
                            Opa5.assert.ok(true, `Dialog with title: '${title}' is displayed.`);
                        } else {
                            let textElement = dialog.getContent()[0];

                            Opa5.assert.equal(textElement.getText(), displayedText, `Dialog with title: '${title}' is displayed.`);
                        }
                    },
                    errorMessage: `No sap.m.Dialog with title '${title} is shown.`
                });
            },

            iShouldSeeElementWithIdAndValueStateError: function(inputId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: inputId,
                    visible: true,
                    matchers: [
                        new PropertyStrictEquals({
                            name: "valueState",
                            value: "Error"                                
                        })
                    ],
                    success: function() {
                        Opa5.assert.ok(true, `Input with id '${inputId}' has valueState Error.`);
                    },
                    errorMessage: `Could not find input with id '${inputId}'.`
                });
            },

            iShouldSeeElementWithBindingAndValueStateError: function(bindingSettings) {
                const OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS),
                        new PropertyStrictEquals({
                            name: "valueState",
                            value: "Error"                                
                        })
                    ],
                    success: function() {
                        Opa5.assert.ok(true, `Input with binding '${JSON.stringify(OPTIONS)}' has valueState Error.`);
                    },
                    errorMessage: `Could not find input with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iShouldSeeElementWithIdAndValueStateNone: function(inputId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: inputId,
                    visible: true,
                    matchers: [
                        new PropertyStrictEquals({
                            name: "valueState",
                            value: "None"                                
                        })
                    ],
                    success: function() {
                        Opa5.assert.ok(true, `Input with id '${inputId}' has valueState None.`);
                    },
                    errorMessage: `Could not find input with id '${inputId}'.`
                });
            },

            iShouldSeeElementWithBindingAndValueStateNone: function(bindingSettings) {
                const OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS),
                        new PropertyStrictEquals({
                            name: "valueState",
                            value: "None"                                
                        })
                    ],
                    success: function() {
                        Opa5.assert.ok(true, `Input with binding '${JSON.stringify(OPTIONS)}' has valueState None.`);
                    },
                    errorMessage: `Could not find input with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iShouldSeeMessageToast: function () {
                return this.getOpa5().waitFor({
                    autoWait: true,
                    check: function () {
                        // message toast received by its assigned class
                        return Opa5.getJQuery()(".sapMMessageToast").length > 0;
                    },
                    success: function () {
                        Opa5.assert.ok(true, "The message toast was shown");
                    },
                    errorMessage: "The message toast did not show up"
               });
            },

            iShouldSeeSelectItemWithLabelFromSelectWithId: function(selectId, itemKey, itemLabel) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: selectId,
                    controlType: "sap.m.Select",
                    visible: true,
                    success: function(select) {
                        Opa5.ok(select !== undefined, `Select with id '${selectId}' displayed.`);

                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            visible: false,
                            matchers: [
                                new Ancestor(select),
                                new Properties({key: itemKey}),
                                new PropertyStrictEquals({
                                    name: "text",
                                    value: itemLabel
                                })
                            ],
                            success: function(selectItems) {
                                const selectItem = selectItems[0];

                                Opa5.assert.equal(selectItem.getText(), itemLabel, `Select item with label: '${itemLabel}' is available.`);
                            },
                            errorMessage: `Could not find element with key: '${itemKey}' in select with id '${selectId}'.`
                        });
                    },
                    errorMessage: `Could not find select with id '${selectId}'.`
                });
            },

            iSelectContainsItemWithLabelFromSelectWithBinding: function(bindingSettings, itemKey, itemLabel) {
                let OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    controlType: "sap.m.Select",
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS)
                    ],
                    success: function(selects) {
                        let select = selects[0];

                        Opa5.assert.ok(select !== undefined, `Select with binding '${JSON.stringify(OPTIONS)}' found.`);

                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            visible: false,
                            matchers: [
                                new Ancestor(select),
                                new Properties({key: itemKey}),
                                new PropertyStrictEquals({
                                    name: "text",
                                    value: itemLabel
                                })
                            ],
                            success: function(selectItems) {
                                const selectItem = selectItems[0];

                                Opa5.assert.equal(selectItem.getText(), itemLabel, `Select item with label: '${itemLabel}' is available.`);
                            },
                            errorMessage: `Could not find element with key: '${itemKey}' in select with binding '${JSON.stringify(OPTIONS)}'.`
                        });
                    },
                    errorMessage: `Could not find select with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iTableRecordCellContentEquals: function(tableId, rowIndex, columnIndex, checkFunction) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: tableId,
                    visible: true,
                    success: function(table) {
                        if(table.getItems().length > rowIndex) {
                            const row = table.getItems()[rowIndex];
                            
                            if(row.getCells().length > columnIndex) {
                                const element = row.getCells()[columnIndex];

                                if(checkFunction && typeof checkFunction === "function") {
                                    const checkResult = checkFunction(element)
                                    const message = checkResult && checkResult.message || "element is not expected";
                                    const isEqual = checkResult && checkResult.isEqual || false;

                                    Opa5.assert.ok(isEqual, message);
                                } else {
                                    Opa5.assert.notOk(true, "could not determine whether element is expected");
                                }
                            } else {
                                Opa5.assert.notOk(true, `Row '${rowIndex}' of table with id '${tableId}' does not contain column '${columnIndex}' num columns: '${row.getCells().length}'`);
                            }
                        } else {
                            Opa5.assert.notOk(true, `Table with id '${tableId}' does not contain expected row '${rowIndex}' num rows: '${table.getItems().length}'`);
                        }
                    },
                    errorMessage: `Could not find table with id '${tableId}' or it does not contain expected number of records.`
                });
            },

            iShouldSeeTableColumnWithLabel(tableId, columnLabel) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: tableId,
                    controlType: "sap.m.Table",
                    visible: true,
                    success: function(table) {
                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            controlType: "sap.m.Column",
                            visible: false,
                            matchers: [
                                new Ancestor(table)
                            ],
                            success: function(columns) {
                                let column = columns.find((columnEntry) => {
                                    if(columnEntry.getVisible()) {
                                        let headerContent = columnEntry.getHeader();
                                        return headerContent.getText && headerContent.getText() === columnLabel;
                                    }
                                });
                                
                                if(column) {
                                    Opa5.assert.ok(true, `The table with id '${tableId}' contains column with text '${columnLabel}'`);
                                } else {
                                    Opa5.assert.ok(false, `The table with id '${tableId}' does not contain column with text '${columnLabel}'`);
                                }
                            },
                            errorMessage: `Table with id '${tableId}' does not contain column`
                        });
                    },
                    errorMessage: `Table with id '${tableId}' not found`
                });
            },

            iShouldNotSeeTableColumnWithLabel(tableId, columnLabel) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: tableId,
                    controlType: "sap.m.Table",
                    visible: true,
                    success: function(table) {
                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            controlType: "sap.m.Column",
                            visible: false,
                            matchers: [
                                new Ancestor(table)
                            ],
                            success: function(columns) {
                                let column = columns.find((columnEntry) => {
                                    let headerContent = columnEntry.getHeader();
                                    return headerContent.getText && headerContent.getText() === columnLabel;
                                });

                                if(column && (!column.getVisible() || table.getHiddenInPopin().includes(column.getImportance()))) {
                                    Opa5.assert.ok(true, `The table with id '${tableId}' does not contain column with text '${columnLabel}'`);
                                } else {
                                    Opa5.assert.ok(false, `The table with id '${tableId}' contains column with text '${columnLabel}'`);
                                }
                            },
                            errorMessage: `Table with id '${tableId}' does not contain column`
                        });
                    },
                    errorMessage: `Table with id '${tableId}' not found`
                });
            },

            iShouldSeeAvatarWithId: function(avatarId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: avatarId,
                    controlType: "sap.m.Avatar",
                    visible: true,
                    success: function() {
                        Opa5.assert.ok(true, `Avatar with id '${avatarId}' is shown.`);
                    },
                    errorMessage: `Could not find avatar with id '${avatarId}'.`
                }); 
            },

            iShouldSeePopOver: function() {
                return this.getOpa5().waitFor({
                    controlType: "sap.m.Popover",
                    visible: true,
                    success: function(popovers) {
                        Opa5.assert.ok(true, "I see a popover")
                    },
                    errorMessage: "No popover is open"
                });
            },

            iShouldNotSeePopOver: function() {
                return this.getOpa5().waitFor({
                    controlType: "sap.m.Popover",
                    visible: false,
                    success: function(popovers) {
                        Opa5.assert.ok(true, "I don't see popover")
                    },
                    errorMessage: "A popover is open"
                });
            },

            iShouldSeeToolbarWithIdContainingElement: function(toolbarId, elementType, elementPropertyOptions) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    controlType: "sap.m.Toolbar",
                    id: toolbarId,
                    visible: true,
                    success: function(toolbar) {
                        Opa5.assert.ok(true, `Toolbar with id '${toolbarId}' found.`);

                        this.getOpa5().waitFor({
                            viewName: this.getViewName(),
                            controlType: elementType,
                            matchers: [
                                new Ancestor(toolbar),
                                new PropertyStrictEquals(elementPropertyOptions)
                            ],
                            success: function(element) {
                                Opa5.assert.ok(true, `I see element with properties '${elementPropertyOptions}' in toolbar with id '${toolbarId}'`);
                            },
                            errorMessage: `Could not find element with property: '${elementPropertyOptions}' in toolbar with id '${toolbarId}'.`
                        });
                    },
                    errorMessage: `toolbar with id '${toolbarId}' not found`
                });
            },

            iShouldSeeStandardListItemWithId: function(listItemId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    id: listItemId,
                    controlType: "sap.m.StandardListItem",
                    visible: true,
                    success: function() {
                        Opa5.assert.ok(true, `StandardListItem with id '${listItemId}' is shown.`);
                    },
                    errorMessage: `Could not find StandardListItem with id '${listItemId}'.`
                }); 
            },

            iItemSelectedFromSelectWithBinding: function(bindingSettings, itemKey) {
                let OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    controlType: "sap.m.Select",
                    visible: true,
                    matchers: [
                        new BindingPath(OPTIONS),
                        new Properties({selectedKey: itemKey})
                    ],
                    success: function(selects) {
                        Opa5.assert.ok(true, `Item with key '${itemKey}' selected from select with binding '${JSON.stringify(OPTIONS)}'.`);
                    },
                    errorMessage: `Could not find select with binding '${JSON.stringify(OPTIONS)}'.`
                });
            },

            iShouldBeOnUrlThatContains: function(url) {
                return this.getOpa5().waitFor({
                    check: function() {
                        return Opa5.getWindow().location.href.includes(url);
                    },
                    success: function() {
                        Opa5.assert.ok(true, `I'm on URL '${Opa5.getWindow().location.href}' which contains '${url}'`);
                    },
                    errorMessage: `Not on URL that contains '${url}': '${Opa5.getWindow().location.href}'`
                });
            },

            iShouldSeePopOverWithId: function(popoverId) {
                return this.getOpa5().waitFor({
                    id: popoverId,
                    viewName: this.getViewName(),
                    controlType: "sap.m.Popover",
                    visible: true,
                    success: function(popover) {
                        Opa5.assert.ok(true, `I see popover with id '${popoverId}'`)
                    },
                    errorMessage: `Could not find popover with id '${popoverId}'.`
                });
            },

            iShouldSeeSliderWithId: function(sliderId) {
                return this.getOpa5().waitFor({
                    id: sliderId,
                    viewName: this.getViewName(),
                    controlType: "sap.m.Slider",
                    visible: true,
                    success: function(slider) {
                        Opa5.assert.ok(true, `I see slider with id '${sliderId}'`)
                    },
                    errorMessage: `Could not find slider with id '${sliderId}'.`
                });
            },

            iShouldSeeElementWithLabelFor: function(label, elementType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: elementType,
                    matchers: [
                        new LabelFor({
                            text: label
                        })
                    ],
                    success: function(elements) {
                        Opa5.assert.ok(true, `The element of type '${elementType}' with label '${label}' is shown.`);
                    },
                    errorMessage: `Could not find element of type '${elementType}' with label '${label}''.`
                });
            },

            iInputWithIdHasValue: function(inputId, value) {
                return this.getOpa5().waitFor({
                    id: inputId,
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Input",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "value",
                            value: value
                        })
                    ],
                    success: function(input) {
                        Opa5.assert.equal(input.getValue(), value, `The input with id '${inputId}' and value '${value}' was found.`)
                    },
                    errorMessage: `Could not find input with id '${inputId}' and value '${value}'.`
                });
            },

            iInputWithBindingHasValue: function(bindingSettings, value) {
                const OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Input",
                    matchers: [
                        new BindingPath(OPTIONS),
                        new PropertyStrictEquals({
                            name: "value",
                            value: value
                        })
                    ],
                    success: function(inputs) {
                        let input = inputs[0];
                        Opa5.assert.equal(input.getValue(), value, `The input with binding information '${JSON.stringify(OPTIONS)}' and value '${value}' was found.`)
                    },
                    errorMessage: `Could not find input with binding '${JSON.stringify(OPTIONS)}' and value '${value}'.`
                });
            },

            iElementWithBindingHasValue: function(bindingSettings, value, elementType) {
                const OPTIONS = bindingSettings? bindingSettings: {};
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: elementType,
                    matchers: [
                        new BindingPath(OPTIONS),
                        new PropertyStrictEquals({
                            name: "value",
                            value: value
                        })
                    ],
                    success: function(elements) {
                        let element = elements[0];
                        Opa5.assert.equal(element.getValue(), value, `The element of type '${elementType}' with binding information '${JSON.stringify(OPTIONS)}' and value '${value}' was found.`)
                    },
                    errorMessage: `Could not find element of type '${elementType}' with binding '${JSON.stringify(OPTIONS)}' and value '${value}'.`
                });
            },

            iElementWithLabelForHasValue: function(label, value, elementType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: elementType,
                    matchers: [
                        new LabelFor({
                            text: label
                        }),
                        new PropertyStrictEquals({
                            name: "value",
                            value: value
                        })
                    ],
                    success: function(elements) {
                        let element = elements[0];
                        Opa5.assert.equal(element.getValue(), value, `The element of type '${elementType}' with label '${label}' and value '${value}' was found.`)
                    },
                    errorMessage: `Could not find element of type '${elementType}' with label '${label}' and value '${value}'.`
                });
            },

            iShouldSeeTitle: function(titleText) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Title",
                    matchers: [new PropertyStrictEquals({
                        name: "text",
                        value: titleText
                    })],
                    success: function(titles) {
                        Opa5.assert.ok(true, `Title with text '${titleText}' is shown.`);
                    },
                    errorMessage: `Could not find title with text '${titleText}'.`
                });
            },

            iShouldSeeObjectPageSectionWithTitle: function(title) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.uxap.ObjectPageSectionBase",
                    matchers: [new PropertyStrictEquals({
                        name: "title",
                        value: title
                    })],
                    success: function(sections) {
                        Opa5.assert.ok(true, `Section with title '${title}' is shown.`);
                    },
                    errorMessage: `Could not find Section with title '${title}'.`
                });
            },

            iShouldSeeTextElementWithText: function(text) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Text",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "text",
                            value: text
                        })
                    ],
                    success: function(elements) {
                        Opa5.assert.ok(true, `The text '${text}' is shown.`);
                    },
                    errorMessage: `Could not find text '${text}'.`
                });
            },

            iShouldSeeAvatarWithSource: function(source) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Avatar",
                    matchers: [new PropertyStrictEquals({
                        name: "src",
                        value: source
                    })],
                    success: function(elements) {
                        Opa5.assert.ok(true, `Avatar is shown`)
                    },
                    errorMessage: `Could not find avatar with src '${source}'.`
                });
            },

            iShouldSeeIconWithSource: function(source) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.ui.core.Icon",
                    matchers: [new PropertyStrictEquals({
                        name: "src",
                        value: source
                    })],
                    success: function(elements) {
                        Opa5.assert.ok(true, `Icon is shown`)
                    },
                    errorMessage: `Could not find Icon with src '${source}'.`
                });
            },

            iShouldSeeButtonWithIcon: function(icon, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "icon",
                            value: icon
                        })
                    ],
                    success: function(buttons) {
                        let button = buttons[0];
                        if(!buttonType) {
                            Opa5.assert.ok(true, `The button with icon '${icon}' is shown.`)
                        } else {
                            Opa5.assert.equal(button.getType(), buttonType, `The button with icon '${icon}' and type '${buttonType}' is shown.`)
                        }
                    },
                    errorMessage: `Could not find button with icon '${icon}'.`
                });
            },

            iShouldNotSeeButtonWithIcon: function(icon) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: false,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "icon",
                            value: icon
                        })
                    ],
                    success: function(buttons) {
                        let button = buttons[0];
                        Opa5.assert.notOk(button.getVisible(), `The button with icon '${icon}' is not visible.`)
                    },
                    errorMessage: `Could not find button with icon '${icon}' or it is visible.`
                });
            },

            iShouldSeeButtonWithIconAndLabel: function(icon, label, buttonType) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    visible: true,
                    controlType: "sap.m.Button",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "icon",
                            value: icon
                        }),
                        new PropertyStrictEquals({
                            name: "text",
                            value: label
                        })
                    ],
                    success: function(buttons) {
                        let button = buttons[0];
                        if(!buttonType) {
                            Opa5.assert.ok(true, `The button with icon '${icon}' and label '${label}' is shown.`)
                        } else {
                            Opa5.assert.equal(button.getType(), buttonType, `The button with icon '${icon}', label '${label}', and type '${buttonType}' is shown.`)
                        }
                    },
                    errorMessage: `Could not find button with icon '${icon}' and label '${label}'.`
                });
            },

            iShouldSeeMessageListItemWithTitleAndDescription: function(title, description, messageType) {
                return this.getOpa5().waitFor({
                    visible: true,
                    controlType: "sap.m.MessageListItem",
                    matchers: [
                        new PropertyStrictEquals({
                            name: "title",
                            value: title
                        }),
                        new PropertyStrictEquals({
                            name: "description",
                            value: description
                        })
                    ],
                    success: function(messageItems) {
                        let messageItem = messageItems[0];

                        Opa5.assert.equal(messageItem.getDescription(), description, `The message item with title '${title}' and description '${description}' is shown.`)

                        if(messageType) {
                            Opa5.assert.equal(messageItem.getMessageType(), messageType, `The message item with title '${title}' and type '${messageType}' is shown.`)
                        }
                    },
                    errorMessage: `Could not find message item with title '${title}' and description '${description}'.`
                });
            },

            iShouldNotSeeToolbarWithId: function(toolbarId) {
                return this.getOpa5().waitFor({
                    viewName: this.getViewName(),
                    controlType: "sap.m.Toolbar",
                    id: toolbarId,
                    visible: false,
                    success: function(toolbar) {
                        Opa5.assert.notOk(toolbar.getVisible(), `Toolbar with id '${toolbarId}' is not visible.`);
                    },
                    errorMessage: `toolbar with id '${toolbarId}' not found.`
                });
            }
        }
    });
});