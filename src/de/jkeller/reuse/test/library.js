sap.ui.define(
    function() {
        "use strict";

        var library = sap.ui.getCore().initLibrary({
            name: "de.jkeller.reuse.test",
            version: "0.1-SNAPSHOT",
            dependencies: [
                "sap.ui.core"
            ],
            interfaces: [
                "de.jkeller.reuse.test.opa.page.BasePage",
                "de.jkeller.reuse.test.actions.SlideHandle"
            ]
        });	
    
        return library; 
    }
);