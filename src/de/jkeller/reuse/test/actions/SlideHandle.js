sap.ui.define([
    "sap/ui/test/actions/Action",
    "sap/base/Log",
], function (Action, Log) {
    "use strict";

    let SlideHandle = Action.extend("de.jkeller.reuse.test.actions.SlideHandle", {
        metadata:{
            properties: {
                /**
                 * The id suffix of the DOM element that represents the overall range of the slider.
                 */
                sliderIdSuffix: {
                    type: "string",
                    defaultValue: "inner"
                },
                /**
                 * The id suffix of the DOM element that represents the (slider) handle. 
                 */
                handleIdSuffix: {
                    type: "string",
                    defaultValue: "handle"
                },
                /**
                 * Number of (x-axis) values the handle shall be moved in horizontal (x-axis) direction.
                 */
                xValueOffset: {
                    type: "int",
                    defaultValue: 0
                },
                /**
                 * Number of (y-axis) values the handle shall be moved in vertical (y-axis) direction.
                 */
                yValueOffset: {
                    type: "int",
                    defaultValue: 0
                },
                /**
                 * The maximum (x-axis) value of the slider. If no value is provided the max property of the control is used.
                 */
                xMax: {
                    type: "int"
                },
                /**
                 * The minimum (x-axis) value of the slider. If no value is provided the min property of the control is used.
                 */
                xMin: {
                    type: "int"
                },
                /**
                 * The maximum (y-axis) value of the slider. If no value is provided the max property of the control is used.
                 */
                yMax: {
                    type: "int"
                },
                /**
                 * The minimum (y-axis) value of the slider. If no value is provided the max property of the control is used.
                 */
                yMin: {
                    type: "int"
                }
            }
        },

        constructor: function (settings) {
            Action.prototype.constructor.call(this, settings);
        },

        init: function () {
            Action.prototype.init.apply(this, arguments);
        },

        executeOn : function (control) {
            let $actionDomRef = this.$(control);
            let actionDomRef = control.getDomRef();
            let sliderDomRef = control.getDomRef(this.getSliderIdSuffix());
            let handleDomRef = control.getDomRef(this.getHandleIdSuffix());

            if (!actionDomRef && !sliderDomRef && !handleDomRef) {
                return;
            }

            if (actionDomRef.disabled) {
                Log.debug("Cannot slide handle " + control + ": control is not enabled!");
                return;
            }

            this._tryOrSimulateFocusin($actionDomRef, control);

            let beginCoordinates = this._getEventCoordinates(sliderDomRef, {position: this.dropPosition.BEFORE});
            let endCoordinates = this._getEventCoordinates(sliderDomRef, {position: this.dropPosition.AFTER});

            let isXValueToBeMoved = !!this.getXValueOffset() != 0;
            let isYValueToBeMoved = !!this.getXValueOffset() != 0;
            let xMax = this.getXMax() !== undefined ? this.getXMax() : isXValueToBeMoved && control.getMax && control.getMax();
            let xMin = this.getXMin() !== undefined ? this.getXMin() : isXValueToBeMoved && control.getMin && control.getMin() || 0;
            let yMax = this.getYMax() !== undefined ? this.getYMax() : isYValueToBeMoved && control.getMax && control.getMax();
            let yMin = this.getYMin() !== undefined ? this.getYMin() : isYValueToBeMoved && control.getMin && control.getMin() || 0;

            let eventCoordinates = {
                x: isXValueToBeMoved ? ((endCoordinates.x - beginCoordinates.x) / (xMax - xMin)) * this.getXValueOffset() : beginCoordinates.x,
                y: isXValueToBeMoved ? ((endCoordinates.y - beginCoordinates.y) / (yMax - yMin)) * this.getYValueOffset() : beginCoordinates.y
            }

            this._createAndDispatchMouseEvent("mousedown", handleDomRef);
            this._createAndDispatchMouseEvent("mousemove", handleDomRef, false, false, false, eventCoordinates.x, eventCoordinates.y);
            this._createAndDispatchMouseEvent("mouseup", handleDomRef);
        }
    });

    return SlideHandle;
});