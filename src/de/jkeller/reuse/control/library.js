sap.ui.define(
    function() {
        "use strict";

        var library = sap.ui.getCore().initLibrary({
            name: "de.jkeller.reuse.control",
            version: "0.1-SNAPSHOT",
            dependencies: [
                "sap.m",
                "sap.ui.layout",
                "sap.ui.core"
            ],
            controls: [
                "de.jkeller.reuse.control.duration.DurationSlider"
            ],
            elements: [
                "de.jkeller.reuse.control.duration.MinMaxScale"
            ]
        });	
    
        return library; 
    }
);