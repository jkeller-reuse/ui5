sap.ui.define([
    "sap/m/InputBase",
    "sap/m/Input",
    "sap/m/ResponsivePopover",
    "sap/m/Slider",
    "sap/ui/layout/form/SimpleForm",
    "sap/m/Label",
    "sap/ui/core/CustomData",
    "sap/base/Log",
    "sap/m/MessageStrip",
    "sap/ui/model/FormatException",
    "sap/ui/model/ValidateException",
    "de/jkeller/reuse/control/duration/MinMaxScale",
    "de/jkeller/reuse/control/duration/TimeUnit"
],

    /**
     * Provide an (input) control capable of dealing with durations including 
     * hours, minutes, seconds, and millis seconds. The control provides a 
     * convenient way for entering durations according to  desired duration 
     * format (e.g., HH:mm:ss.SSS).
     * @param {@typeof sap.m.InputBase} InputBase
     * @param {@typeof sap.m.Input} Input
     * @param {@typeof sap.m.ResponsivePopover} ResponsivePopover
     * @param {@typeof sap.m.Slider} Slider
     * @param {@typeof sap.ui.layout.form.SimpleForm} SimpleForm
     * @param {@typeof sap.m.Label} Label
     * @param {@typeof sap.ui.core.CustomData} CustomData
     * @param {@typeof sap.base.Log} Log
     * @param {@typeof sap.m.MessageStrip} MessageStrip
     * @param {@typeof sap.ui.model.FormatException} FormatException
     * @param {@typeof sap.ui.model.ValidateException} ValidateException
     * @param {@typeof de.jkeller.reuse.control.duration.MinMaxScale} MinMaxScale
     * @param {@typeof de.jkeller.reuse.control.duration.TimeUnit} TimeUnit
     */
    function (InputBase, Input, ResponsivePopover, Slider, SimpleForm, Label, CustomData, Log, MessageStrip, FormatException,
              ValidateException, MinMaxScale, TimeUnit) {
        "use strict";

        const nonWordRegex = /\W+/g;
        const wordRegex = /\w+/g;
        const TIME_FORMAT_DEFAULT = "HH:mm:ss.SSS";
        const MIN_DEFAULT = "00:00:00.000";
        const MAX_DEFAULT = "23:59:59.999";

        return InputBase.extend("de.jkeller.reuse.control.duration.DurationSlider", {
            metadata : {
                properties : {
                    timeFormat: {type: "string", defaultValue: TIME_FORMAT_DEFAULT},
                    min: {type: "string", defaultValue: MIN_DEFAULT},
                    max: {type: "string", defaultValue: MAX_DEFAULT}
                },
                aggregations : {
                    _input : {type : "sap.m.Input", multiple: false, visibility : "hidden"},
                    _popover : {type : "sap.m.ResponsivePopover", multiple: false, visibility : "hidden"},
                },
                events: {
                    change: {
                        parameters: {
                            value: {type: "string"}
                        }
                    }
                }
            },

            constructor: function(id, settings) {
                InputBase.apply(this, [id, settings]);
                this._isConstructed = true;

                const options = settings || typeof id === "object" ? id : {};
                if(!options.timeFormat) {
                    this.setTimeFormat(this.getProperty("timeFormat"));
                } else {
                    this.setMin(this.getProperty("min"));
                    this.setMax(this.getProperty("max"));
                }
            },

            init: function () {
                this._resourceBundle = sap.ui.getCore().getLibraryResourceBundle("de.jkeller.reuse.control");

                const ID = this.getId() ? this.getId() : "";
                this.setAggregation("_input", new Input({
                    id: ID + "--input",
                    showValueHelp: true,
                    valueHelpIconSrc: "sap-icon://fob-watch",
                    valueHelpRequest: this._onValueHelpRequest.bind(this),
                    change: this._onInputChange.bind(this)
                }));

                this.setAggregation("_popover", new ResponsivePopover({
                    id: ID + "--popover",
                    visible: false,
                    showArrow: false,
                    scrollable: false,
                    placement: sap.m.PlacementType.VerticalPreferredBottom,
                    contentWidth: "30rem"
                }));

                this._minHours = 0;
                this._minMinutes = 0;
                this._minSeconds = 0;
                this._minMillis = 0;
            },

            renderer: function (renderManager, control) {
                renderManager.openStart("div", control.getId());
                renderManager.openEnd();
                    renderManager.renderControl(control.getAggregation("_input"));
                    renderManager.renderControl(control.getAggregation("_popover"));
                renderManager.close("div");
            },

            onAfterRendering: function(event) {
                this.getAggregation("_popover").setVisible(true);
            },

            _createSlider: function(unit) {
                let minValue = 0;
                let maxValue = 9;
                switch(unit.getIdentifier()) {
                    case TimeUnit.Identifier.Hours:
                        minValue = this._getMinHours();
                        maxValue = this._getMaxHours();
                        break;
                    case TimeUnit.Identifier.Minutes:
                        minValue = this._getMinMinutes();
                        maxValue = this._getMaxMinutes();
                        break;
                    case TimeUnit.Identifier.Seconds:
                        minValue = this._getMinSeconds();
                        maxValue = this._getMaxSeconds();
                        break;
                    case TimeUnit.Identifier.Millis:
                        minValue = this._getMinMillis();
                        maxValue = this._getMaxMillis();
                        break;
                }

                const slider = new Slider({
                    id: this.getId() + "--" + unit.getIdentifier() + "-slider",
                    showAdvancedTooltip: true,
                    inputsAsTooltips: true,
                    showHandleToolTip: false,
                    enableTickmarks: true,
                    scale: new MinMaxScale(),
                    step: 1,
                    min: minValue,
                    max: maxValue,
                    customData: new CustomData({key: "unit", value: unit}),
                    change: this._onSliderChange.bind(this)
                });
                slider.addStyleClass("sapUiMediumMarginBottom");

                this._addTimeUnitSlider(unit.getIdentifier(), slider);

                return slider;
            },

            _isValidTimeFormat: function(timeFormat) {
                const validFormatRegex = /^[HhmsS\W]+$/g;

                if(!validFormatRegex.test(timeFormat)) {
                    Log.error(`time format '${timeFormat}' contains invalid identifiers (allowed 'HhmsS').`);
                    return false;
                } else if(timeFormat.includes("-")) {
                    Log.error(`time format '${timeFormat}' contains invalid character '-' (which may indicate negative durations which aren't allowed).`);
                    return false;
                }

                let unitIdentifiers = {};
                const timeFormatParts = timeFormat.split(/\W/g) || [];
                timeFormatParts.forEach((timeFormatPart) => {
                    const character = timeFormatPart.charAt(0);
                    const unitIdentifier = this._getUnitIdentifier(character);
                    if(character && !new RegExp("^" + character + "+$").test(timeFormatPart)) {
                        Log.error(`time format '${timeFormat}' contains identifiers not correctly separated by special characters.`);
                        return false;
                    }
                    if(unitIdentifiers.hasOwnProperty(unitIdentifier)) {
                        Log.error(`time format '${timeFormat}' contains unit identifier '${unitIdentifier}' multiple times, which isn't supported.`);
                        return false;
                    }
                    unitIdentifiers[unitIdentifier] = true;
                });
                
                return true;
            },

            _isHourIdentifier: function(value) {
                return value && typeof value === "string" 
                    && (value.includes(TimeUnit.Identifier.Hours) || value.includes(TimeUnit.Identifier.Hours.toLowerCase()));
            },

            _isMinuteIdentifier: function(value) {
                return value && typeof value === "string" && value.includes(TimeUnit.Identifier.Minutes);
            },

            _isSecondIdentifier: function(value) {
                return value && typeof value === "string" && value.includes(TimeUnit.Identifier.Seconds);
            },

            _isMillisIdentifier: function(value) {
                return value && typeof value === "string" && value.includes(TimeUnit.Identifier.Millis);
            },

            _getUnitIdentifier: function(value) {
                let unitIdentifier;
                Object.keys(TimeUnit.Identifier).forEach((key) => {
                    const identifier = TimeUnit.Identifier[key];
                    if(value && typeof value === "string" && value.includes(identifier)) {
                        unitIdentifier = identifier;
                        return;
                    }
                });

                return unitIdentifier;
            },

            /**
             * Sets the desired time (duration) format. This will trigger the 
             * (re-) building of available value help for entering time unit 
             * parts, e.g., one for hours, minutes etc., according to provided 
             * format. Furthermore, the format must be (re-) analyzed, e.g., 
             * for providing correct checks for valid inputted values.
             * @public
             * @param {@tpyeof string} value representing the desired duration 
             *  format (e.g., HH:mm:ss.SSS)
             */
            setTimeFormat: function(value) {
                if(!this._isValidTimeFormat(value)) {
                    return;
                }

                this.setProperty("timeFormat", value, true);

                if(!this.getPlaceholder()) {
                    this.setPlaceholder(value);
                }
                this._setTimeFormatParts();
                this._setTimeFormatDelimiters();
                this._setInputValidFormatRegex();

                this.setMin(this.getProperty("min"));
                this.setMax(this.getProperty("max"));

                const popover = this.getAggregation("_popover");
                popover.destroyContent();
                this._clearTimeUnitSliders();

                const unitSliders = [];
                this._getTimeFormatParts().forEach((timeFormatPart) => {
                    if(this._isHourIdentifier(timeFormatPart)) {
                        unitSliders.push(new Label({text: this._getResourceBundle().getText("durationSlider-unit-hours")}));
                        unitSliders.push(this._createSlider(TimeUnit.Hours()));
                        this._addUnit(TimeUnit.Hours());
                    } else if(this._isMinuteIdentifier(timeFormatPart)) {
                        unitSliders.push(new Label({text: this._getResourceBundle().getText("durationSlider-unit-minutes")}));
                        unitSliders.push(this._createSlider(TimeUnit.Minutes()));
                        this._addUnit(TimeUnit.Minutes());
                    } else if(this._isSecondIdentifier(timeFormatPart)) {
                        unitSliders.push(new Label({text: this._getResourceBundle().getText("durationSlider-unit-seconds")}));
                        unitSliders.push(this._createSlider(TimeUnit.Seconds()));
                        this._addUnit(TimeUnit.Seconds());
                    } else if(this._isMillisIdentifier(timeFormatPart)) {
                        unitSliders.push(new Label({text: this._getResourceBundle().getText("durationSlider-unit-millis")}));
                        unitSliders.push(this._createSlider(TimeUnit.Millis()));
                        this._addUnit(TimeUnit.Millis());
                    }
                });

                this._setPopoverValueStateTextElement(new MessageStrip({
                    visible:false,
                }));
                popover.addContent(this._getPopoverValueStateTextElement());

                popover.addContent(new SimpleForm({
                    editable: true,
                    layout: "ResponsiveGridLayout" ,
                    labelSpanS: 3,
                    content: unitSliders
                }));
            },

            /**
             * @private
             * @param {@typeof string} timeFormatPart 
             * @returns the overall available maximum for provided timeFormatPart,
             * e.g., if timeFormatPart is mmm then 999 is overall maximum
             */
            _getOverallMaxForTimePart: function(timeFormatPart) {
                return Math.pow(10, timeFormatPart.length + 1) - 1 - 9*Math.pow(10, timeFormatPart.length);
            },

            _setUnitMax: function(timeFormatPart, value) {
                if(this._isHourIdentifier(timeFormatPart)) {
                    this._setMaxHours(value, timeFormatPart);
                } else if(this._isMinuteIdentifier(timeFormatPart)) {
                    this._setMaxMinutes(value, timeFormatPart);
                } else if(this._isSecondIdentifier(timeFormatPart)) {
                    this._setMaxSeconds(value, timeFormatPart);
                } else if(this._isMillisIdentifier(timeFormatPart)) {
                    this._setMaxMillis(value, timeFormatPart);
                }
            },

            _setUnitMin: function(timeFormatPart, value) {
                if(this._isHourIdentifier(timeFormatPart)) {
                    this._setMinHours(value)
                } else if(this._isMinuteIdentifier(timeFormatPart)) {
                    this._setMinMinutes(value);
                } else if(this._isSecondIdentifier(timeFormatPart)) {
                    this._setMinSeconds(value);
                } else if(this._isMillisIdentifier(timeFormatPart)) {
                    this._setMinMillis(value);
                }
            },

            _onValueHelpRequest: function() {
                const popover = this.getAggregation("_popover");
                if(popover.isOpen()) {
                    popover.close();
                } else {
                    popover.openBy(this.getAggregation("_input"));
                }
            },

            setPlaceholder: function(value) {
                this.getAggregation("_input").setPlaceholder(value);
                this.setProperty("placeholder", value, true);
            },

            getPlaceholder: function() {
                return this.getAggregation("_input").getPlaceholder()
            },

            _setPropertyIfGreaterEqualsZero: function(value, propertyKey) {
                if(value >= 0 && this._isNumber(value)) {
                    this[propertyKey] = value;
                } else {
                    this[propertyKey] = 0;
                }
            },

            _setUnitMinProperty: function(value, propertyKey, unitIdentifier) {
                if(this._isHighestRankUnit(unitIdentifier)) {
                    this._setPropertyIfGreaterEqualsZero(value, propertyKey);
                } else {
                    this[propertyKey] = 0;
                }
            },

            _setTimeUnitSliderMax: function(timeFormatPart, max) {
                const unitIdentifier = this._getUnitIdentifier(timeFormatPart);
                const slider = this._getTimeUnitSlider(unitIdentifier);
                if(!slider) {
                    return;
                }
                
                let min;
                if(max < this.getMin()) {
                    min = this.getMin();
                }

                const newMax = min ? min : max;
                slider.setMax(newMax);
                if(min) {
                    slider.setMin(max);
                }
            },

            _setTimeUnitSliderMin: function(timeFormatPart, min) {
                const unitIdentifier = this._getUnitIdentifier(timeFormatPart);
                const slider = this._getTimeUnitSlider(unitIdentifier);
                if(!slider) {
                    return;
                }
                
                let max;
                if(min > this.getMax()) {
                    max = this.getMax();
                }

                const newMin = max ? max : min;
                slider.setMin(newMin);
                if(max) {
                    slider.setMax(min);
                }
            },

            _setMinHours: function(minHours) {
                this._setUnitMinProperty(minHours, "_minHours", TimeUnit.Identifier.Hours);
                this._setTimeUnitSliderMin(TimeUnit.Identifier.Hours, this._minHours);
            },

            _setMinMinutes: function(minMinutes) {
                this._setUnitMinProperty(minMinutes, "_minMinutes", TimeUnit.Identifier.Minutes);
                this._setTimeUnitSliderMin(TimeUnit.Identifier.Minutes, this._minMinutes);
            },

            _setMinSeconds: function(minSeconds) {
                this._setUnitMinProperty(minSeconds, "_minSeconds", TimeUnit.Identifier.Seconds);
                this._setTimeUnitSliderMin(TimeUnit.Identifier.Seconds, this._minSeconds);
            },

            _setMinMillis: function(minMillis) {
                this._setUnitMinProperty(minMillis, "_minMillis", TimeUnit.Identifier.Millis);
                this._setTimeUnitSliderMin(TimeUnit.Identifier.Millis, this._minMillis);
            },

            _getMinHours: function() {
                return this._minHours;
            },

            _getMinMinutes: function() {
                return this._minMinutes;
            },

            _getMinSeconds: function() {
                return this._minSeconds;
            },

            _getMinMillis: function() {
                return this._minMillis;
            },

            _setMaxHours: function(maxHours, timeFormatPart) {
                let value = this._isNumber(maxHours) && this._isHighestRankUnit(TimeUnit.Identifier.Hours) ? maxHours : undefined;
                const overAllMaxHours = this._getOverallMaxForTimePart(timeFormatPart);
                if(timeFormatPart !== undefined && (value === undefined || value > overAllMaxHours)) {
                    value = timeFormatPart.length === 2 ? 23 : overAllMaxHours;
                }

                this._maxHours = value;
                this._setTimeUnitSliderMax(TimeUnit.Identifier.Hours, value);
            },

            _setMaxMinutes: function(maxMinutes, timeFormatPart) {
                let value = this._isNumber(maxMinutes) && this._isHighestRankUnit(TimeUnit.Identifier.Minutes) ? maxMinutes : undefined;
                const overAllMaxMinutes = this._getOverallMaxForTimePart(timeFormatPart);
                if(timeFormatPart !== undefined && (value === undefined || value > overAllMaxMinutes)) {
                    value = timeFormatPart.length === 2 ? 59 : overAllMaxMinutes;
                }

                this._maxMinutes = value;
                this._setTimeUnitSliderMax(TimeUnit.Identifier.Minutes, value);
            },

            _setMaxSeconds: function(maxSeconds, timeFormatPart) {
                let value = this._isNumber(maxSeconds) && this._isHighestRankUnit(TimeUnit.Identifier.Seconds) ? maxSeconds : undefined;
                const overAllMaxSeconds = this._getOverallMaxForTimePart(timeFormatPart);
                if(timeFormatPart !== undefined && (value === undefined || value > overAllMaxSeconds)) {
                    value = timeFormatPart.length === 2 ? 59 : overAllMaxSeconds;
                }

                this._maxSeconds = value;
                this._setTimeUnitSliderMax(TimeUnit.Identifier.Seconds, value);
            },

            _setMaxMillis: function(maxMillis, timeFormatPart) {
                let value = this._isNumber(maxMillis) && this._isHighestRankUnit(TimeUnit.Identifier.Millis) ? maxMillis : undefined;
                const overAllMaxMillis = this._getOverallMaxForTimePart(timeFormatPart);
                this._maxMillis = value !== undefined && value <= overAllMaxMillis ? value : overAllMaxMillis;
                this._setTimeUnitSliderMax(TimeUnit.Identifier.Millis, this._maxMillis);
            },

            _getMaxHours: function() {
                return this._maxHours;
            },

            _getMaxMinutes: function() {
                return this._maxMinutes;
            },

            _getMaxSeconds: function() {
                return this._maxSeconds;
            },

            _getMaxMillis: function() {
                return this._maxMillis;
            },

            /**
             * analyze stored time (duration) format and fill respective list 
             * of time format parts, i.e., different parts of the format corresponding 
             * to time (units) (everything that is not considered as delimiter).
             */
            _setTimeFormatParts: function() {
                const timeFormat = this.getTimeFormat();
                const value = timeFormat && typeof timeFormat === "string" ? timeFormat : "";
                this._timeFormatParts = value.match(wordRegex).filter((element) => !!element) || [];

                this._highestRankIdentifier = undefined;
                this._timeUnitData = {};
                this._timeFormatParts.forEach((timeFormatPart) => {
                    const identifier = this._getUnitIdentifier(timeFormatPart);
                    const startIndex = timeFormat.indexOf(timeFormatPart);
                    const indices = {
                        startIndex: startIndex,
                        endIndex: startIndex + timeFormatPart.length
                    };
                    this._timeUnitData[identifier] = {valueIndices: indices};

                    if(!this._highestRankIdentifier) {
                        this._highestRankIdentifier = identifier;
                    } else if(TimeUnit.compareRank(this._highestRankIdentifier, identifier) > 0) {
                        this._highestRankIdentifier = identifier;
                    }
                });
            },

            _getTimeFormatParts: function() {
                const timeFormatParts = this._timeFormatParts;
                return timeFormatParts ? timeFormatParts : [];
            },

            _setTimeFormatDelimiters: function() {
                const timeFormat = this.getTimeFormat();
                const value = timeFormat && typeof timeFormat === "string" ? timeFormat : "";
                this._timeFormatDelimiters = value.match(nonWordRegex) || [];
                if(/\W+$/g.test(timeFormat)) {
                    this._timeFormatDelimiters = this._timeFormatDelimiters.slice(0, this._timeFormatDelimiters.length - 1);
                }
                if(/^\W+/g.test(timeFormat)) {
                    this._timeFormatDelimiters = this._timeFormatDelimiters.slice(1, this._timeFormatDelimiters.length);
                }
            },

            _getTimeFormatDelimiters: function() {
                const timeFormatDelimiters = this._timeFormatDelimiters;
                return timeFormatDelimiters ? timeFormatDelimiters : [];
            },

            _isNumber: function(value) {
                return !!parseInt(value, 10);
            },

            setMin: function(min) {
                if(typeof min === "string") {
                    if(this._isConstructed) {
                        const minValueParts = min.split(nonWordRegex) || [];
                        const timeFormatParts = this._getTimeFormatParts();
                        let shallFallbackToDefaults = false;
                        let fallbackDefaultMin;

                        if(timeFormatParts.length !== minValueParts.length) {
                            Log.error(`length of provided min value '${min}' isn't compatible with time format '${this.getTimeFormat()}'`);
                            shallFallbackToDefaults = true;
                            fallbackDefaultMin = this.getTimeFormat();
                        }
                        
                        this._minInMillis = BigInt(0);
                        for(let i = 0; i < timeFormatParts.length; ++i) {
                            const timeFormatPart = timeFormatParts[i];
                            const minValuePart = minValueParts[i];
                            const unitIdentifier = this._getUnitIdentifier(timeFormatPart);
                            let minValue = this._isNumber(minValuePart) ? parseInt(minValuePart, 10) : 0;
                            if(minValue < 0) {
                                minValue = 0;
                                Log.error("min value for part '" + timeFormatPart + "' not valid, min must be strict greater 0!");
                            } else if(minValue > this._getUnitMax(timeFormatPart)) {
                                const maxValue = this._getUnitMax(timeFormatPart);
                                this._setUnitMax(timeFormatPart, minValue);
                                minValue = maxValue;
                            }

                            this._setUnitMin(timeFormatPart, minValue);
                            this._minInMillis += TimeUnit.get(unitIdentifier).toMillis(minValue);

                            if(shallFallbackToDefaults) {
                                fallbackDefaultMin = this._replaceUnitPart(fallbackDefaultMin, unitIdentifier, minValue);
                            }
                        }

                        this.setProperty("min", !fallbackDefaultMin ? min : fallbackDefaultMin, true);
                    } else {
                        this.setProperty("min", min, true);
                    }
                }
            },

            setMax: function(max) {
                if(typeof max === "string") {
                    if(this._isConstructed) {
                        const maxValueParts = max.split(nonWordRegex) || [];
                        const timeFormatParts = this._getTimeFormatParts();
                        let shallFallbackToDefaults = false;
                        let fallbackDefaultMax;
                        
                        if(timeFormatParts.length !== maxValueParts.length) {
                            Log.error(`length of provided max value '${max}' isn't compatible with time format '${this.getTimeFormat()}'`);
                            shallFallbackToDefaults = true;
                            fallbackDefaultMax = this.getTimeFormat();
                        }

                        this._maxInMillis = BigInt(0);
                        for(let i = 0; i < timeFormatParts.length; ++i) {
                            const timeFormatPart = timeFormatParts[i];
                            const unitIdentifier = this._getUnitIdentifier(timeFormatPart);
                            const maxValuePart = !shallFallbackToDefaults ? maxValueParts[i] : undefined;
                            let maxValue = this._isNumber(maxValuePart) ? parseInt(maxValuePart, 10) : undefined;
                            
                            if(maxValue < this._getUnitMin(unitIdentifier)) {
                                const minValue = this._getUnitMin(timeFormatPart);
                                this._setUnitMin(timeFormatPart, maxValue);
                                maxValue = minValue;
                            }
                            
                            this._setUnitMax(timeFormatPart, maxValue);
                            const unitMaxValue = !fallbackDefaultMax ? maxValue : this._getUnitMax(unitIdentifier);
                            this._maxInMillis += TimeUnit.get(unitIdentifier).toMillis(unitMaxValue);

                            if(shallFallbackToDefaults) {
                                fallbackDefaultMax = this._replaceUnitPart(fallbackDefaultMax, unitIdentifier, unitMaxValue);
                            }
                        }

                        this.setProperty("max", !fallbackDefaultMax ? max : fallbackDefaultMax, true);
                    } else {
                        this.setProperty("max", max, true);
                    }
                }
            },

            _setValue: function(value, doUpdateSliders) {
                this.getAggregation("_input").setValue(value);
                if(this._isConstructed && !!doUpdateSliders) {
                    this._validateValue(value);

                    this._updateSlidersValueViaValue(value);

                    this.fireEvent("change", {
                        value: this.getValue()
                    });
                }
                this.setProperty("value", value, true);
            },

            setValue: function(value) {
                this._setValue(value, true);
            },

            getValue: function() {
                return this.getAggregation("_input").getValue();
            },

            _replaceUnitPart(value, unitIdentifier, unitValue) {
                const unitIndices = this._getUnitValueIndices(unitIdentifier);
                const valuePart = unitValue.toString().padStart(unitIndices.endIndex - unitIndices.startIndex, "0");
                const beforePart = value.substring(0, unitIndices.startIndex); 
                const afterPart = value.substring(unitIndices.endIndex);
                return beforePart + valuePart + afterPart;
            },

            _updateValueViaSliders(unitIdentifier, unitValue) {
                let newValue = this.getValue();
                if(!this.getValue() || !this._getInputValidFormatRegex().test(this.getValue())) {
                    newValue = this.getTimeFormat();
                    Object.keys(this._timeUnitData).forEach((identifier) => {
                        if(identifier === unitIdentifier) {
                            return;
                        }

                        let minValue = 0;
                        switch(identifier) {
                            case TimeUnit.Identifier.Hours:
                                minValue = this._getMinHours();
                                break;
                            case TimeUnit.Identifier.Minutes:
                                minValue = this._getMinMinutes();
                                break;
                            case TimeUnit.Identifier.Seconds:
                                minValue = this._getMinSeconds();
                                break;
                            case TimeUnit.Identifier.Millis:
                                minValue = this._getMinMillis();
                                break;
                        }

                        newValue = this._replaceUnitPart(newValue, identifier, minValue);
                    });
                }

                newValue = this._replaceUnitPart(newValue, unitIdentifier, unitValue);
                this._validateValue(newValue);
                this._setValue(newValue, false);
            },

            /**
             * @public
             * @returns {@typeof BigInt} the stored duration (value that is input) 
             * in millis
             */
            getDurationMillis: function() {
                const value = this.getValue();
                if(!value) {
                    return BigInt(0);
                }
                
                let durationMillis = BigInt(0);
                Object.keys(this._timeUnitData).forEach((identifier) => {
                    const unitIndices = this._getUnitValueIndices(identifier);
                    const unitValue = parseInt(value.substring(unitIndices.startIndex, unitIndices.endIndex + 1), 10);
                    if(unitValue && !isNaN(unitValue)) {
                        durationMillis += TimeUnit.get(identifier).toMillis(unitValue);
                    }
                });

                return durationMillis;
            },

            _setInputValidFormatRegex: function() {
                let regexString = this.getTimeFormat();
                const timeFormatParts = this._getTimeFormatParts();
                const timeFormatDelimiters = this._getTimeFormatDelimiters();

                for(let i = 0; i < timeFormatParts.length; ++i) {
                    const timeFormatPart = timeFormatParts[i];
                    const delimiter = i < timeFormatDelimiters.length ? timeFormatDelimiters[i] : "";

                    let timePartRegex = timeFormatPart.replaceAll(timeFormatPart[0], "\\d");
                    for(let j = 0; j < delimiter.length; ++j) {
                        timePartRegex += "\\" + delimiter[j];
                    }

                    regexString = regexString.replace(timeFormatPart + delimiter, timePartRegex);
                }

                this._inputValidFormatRegex = new RegExp("^" + regexString + "$");
            },

            _getInputValidFormatRegex: function() {
                return this._inputValidFormatRegex;
            },

            setValueState: function(valueState) {
                this.getAggregation("_input").setValueState(valueState);
                this._setPopoverValueState(valueState);
                this.setProperty("valueState", valueState, true);
            },

            getValueState: function() {
                return this.getAggregation("_input").getValueState();
            },

            setValueStateText: function(text) {
                this.getAggregation("_input").setValueStateText(text);
                this._setPopoverValueStateText(text);
                this.setProperty("valueStateText", text, true);
            },

            getValueStateText: function() {
                return this.getAggregation("_input").getValueStateText();
            },

            _setPopoverValueStateTextElement: function(element) {
                this._popoverValueStateMessageStrip = element;
            },

            _getPopoverValueStateTextElement: function() {
                return this._popoverValueStateMessageStrip;
            },

            _setPopoverValueState: function(valueState) {
                const messageStrip = this._getPopoverValueStateTextElement();
                if(messageStrip) {
                    let type;
                    switch(valueState) {
                        case sap.ui.core.ValueState.Error:
                            type = sap.ui.core.MessageType.Error;
                            break;
                        case sap.ui.core.ValueState.Information:
                            type = sap.ui.core.MessageType.Information;
                            break;
                        case sap.ui.core.ValueState.None:
                            type = sap.ui.core.MessageType.None;
                            break;
                        case sap.ui.core.ValueState.Success:
                            type = sap.ui.core.MessageType.Success;
                            break;
                        case sap.ui.core.ValueState.Warning:
                            type = sap.ui.core.MessageType.Warning;
                            break;
                        default:
                            type = sap.ui.core.MessageType.None
                    }

                    messageStrip.setType(type);
                }
            },

            _setPopoverValueStateText: function(text) {
                const messageStrip = this._getPopoverValueStateTextElement();
                if(!messageStrip) {
                    return;
                }

                messageStrip.setText(text);
                messageStrip.setVisible(!!text && this.getShowValueStateMessage());
            },

            _addTimeUnitSlider: function(unit, slider) {
                if(!this.hasOwnProperty("_timeUnitData")) {
                    this._timeUnitData = {};
                    this._timeUnitData[unit] = {};
                }

                this._timeUnitData[unit].slider = slider;
            },

            _clearTimeUnitSliders: function() {
                if(!this._timeUnitData) {
                    Object.values(this._timeUnitData).forEach((entry) => {
                        delete entry.slider;
                    });
                }
            },

            _getTimeUnitSlider: function(unit) {
                return this._timeUnitData && this._timeUnitData.hasOwnProperty(unit) && this._timeUnitData[unit].slider 
                    ? this._timeUnitData[unit].slider : undefined;
            },

            /**
             * @public
             * @returns {@typeof [sap.m.Slider]} array of the time unit sliders 
             * provided in value help to input different parts, e.g., one 
             * representing hours, and seconds.
             */
            getTimeUnitSliders: function() {
                return this._timeUnitData && Object.entries(this._timeUnitData).reduce((sliders, [key, value]) => {
                    if(value.slider) {
                        sliders.push(value.slider);
                    }

                    return sliders;
                }, []) || [];
            },

            _setTimeUnitSliderValue: function(timeFormatPart, value) {
                const unitIdentifier = this._getUnitIdentifier(timeFormatPart);
                const slider = this._getTimeUnitSlider(unitIdentifier);
                if(!slider) {
                    return;
                }

                let newValue = value;
                if(typeof value === "string") {
                    const unitIndices = this._getUnitValueIndices(unitIdentifier);
                    newValue = parseInt(value.substring(0, unitIndices.endIndex - unitIndices.startIndex), 10);
                }

                slider.setValue(!isNaN(newValue) ? newValue : slider.getMin());
            },

            _updateSlidersValueViaValue: function(value) {
                if(!value || !this._getInputValidFormatRegex().test(value)) {
                    this._clearTimeUnitSlidersValue();
                    return;
                }

                let remainingValue = this._stripLeadingAndTrailingNonWords(value);
                const timeFormatParts = this._getTimeFormatParts();
                const delimiters = this._getTimeFormatDelimiters();
                for(let i = delimiters.length - 1; i > -1; --i) {
                    const delimiter = delimiters[i];
                    const valueParts = remainingValue.split(delimiter);
                    remainingValue = valueParts.slice(0, valueParts.length - 1).join(delimiter);
                    
                    this._setTimeUnitSliderValue(timeFormatParts[i + 1], valueParts[valueParts.length - 1]);

                    if(i === 0 && valueParts.length > 1) {
                        this._setTimeUnitSliderValue(timeFormatParts[i], valueParts[i]);
                    }

                    if(valueParts.length === 1) {
                        break;
                    }
                }
            },

            _clearTimeUnitSlidersValue: function() {
                this.getTimeUnitSliders().forEach((slider) => {
                    slider.setValue(slider.getMin());
                });
            },

            _stripLeadingAndTrailingNonWords: function(value) {
                return value && typeof value === "string" ? value.replaceAll(/^\W+|\W+$/g, "") : "";
            },

            _getUnitMin: function(unit) {
                const unitIdentifier = this._getUnitIdentifier(unit);
                switch(unitIdentifier) {
                    case TimeUnit.Identifier.Hours:
                        return this._getMinHours();
                    case TimeUnit.Identifier.Minutes:
                        return this._getMinMinutes();
                    case TimeUnit.Identifier.Seconds:
                        return this._getMinSeconds();
                    case TimeUnit.Identifier.Millis:
                        return this._getMinMillis();
                    default:
                        return 0;
                }
            },

            _getUnitMax: function(unit) {
                const unitIdentifier = this._getUnitIdentifier(unit);
                switch(unitIdentifier) {
                    case TimeUnit.Identifier.Hours:
                        return this._getMaxHours();
                    case TimeUnit.Identifier.Minutes:
                        return this._getMaxMinutes();
                    case TimeUnit.Identifier.Seconds:
                        return this._getMaxSeconds();
                    case TimeUnit.Identifier.Millis:
                        return this._getMaxMillis();
                }
            },

            _getUnitValueIndices(unitIdentifier) {
                return this._timeUnitData && this._timeUnitData.hasOwnProperty(unitIdentifier) && this._timeUnitData[unitIdentifier].valueIndices
                    ? this._timeUnitData[unitIdentifier].valueIndices : {startIndex: 0, endIndex: 0};
            },

            _validateValueInRange: function(value) {
                const valueToValidate = value !== undefined ? value : this.getValue();
                let valueState = sap.ui.core.ValueState.None;
                let valueStateText = "";
                let remainingValue = this._stripLeadingAndTrailingNonWords(valueToValidate);
                const timeFormatParts = this._getTimeFormatParts();

                const numTimeFormatParts = timeFormatParts.length;
                let valueInMillis = BigInt(0);
                for(let i = 0; i < numTimeFormatParts; ++i) {
                    const timeFormatPart = timeFormatParts[i];
                    const unitIdentifier = this._getUnitIdentifier(timeFormatPart);
                    const unitIndices = this._getUnitValueIndices(unitIdentifier);
                    const unitValue = parseInt(remainingValue.substring(0, unitIndices.endIndex - unitIndices.startIndex), 10);
                    remainingValue = this._stripLeadingAndTrailingNonWords(remainingValue.substring(timeFormatPart.length));
                    valueInMillis += TimeUnit.get(unitIdentifier).toMillis(unitValue);

                    if(valueInMillis > this._getMaxInMillis()) {
                        valueState = sap.ui.core.ValueState.Error;
                        valueStateText = this._getResourceBundle()
                            .getText("durationSlider-max-exceeded", [value, this.getMax()]);
                        break;
                    } else if(i === (numTimeFormatParts - 1) && valueInMillis < this._getMinInMillis()) {
                        valueState = sap.ui.core.ValueState.Error;
                        valueStateText = this._getResourceBundle()
                            .getText("durationSlider-min-undercut", [value, this.getMin()]);
                        break;
                    }
                }

                if(valueState === sap.ui.core.ValueState.Error) {
                    this.fireValidationError({
                        element: this,
                        exception: new ValidateException(valueStateText),
                        id: this.getId(),
                        message: valueStateText,
                        property: "value",
                        newValue: valueToValidate
                    });
                } else {
                    this.fireValidationSuccess({
                        element: this,
                        id: this.getId(),
                        property: "value",
                        newValue: valueToValidate
                    });
                }

                return {valueState: valueState, valueStateText: valueStateText};
            },

            _validateValue: function(value) {
                const valueToValidate = value !== undefined ? value : this.getValue();
                let valueState = sap.ui.core.ValueState.None;
                let valueStateText = "";
                if(!valueToValidate) {
                    valueState = sap.ui.core.ValueState.None;
                    valueStateText = undefined;
                    this.fireValidationSuccess({
                        element: this,
                        id: this.getId(),
                        property: "value",
                        newValue: valueToValidate
                    });
                } else if(!this._getInputValidFormatRegex().test(valueToValidate)) {
                    valueState = sap.ui.core.ValueState.Error;
                    valueStateText = this._getResourceBundle()
                        .getText("durationSlider-formatError-invalidInput", [valueToValidate, this.getTimeFormat()]);
                    this.fireFormatError({
                        element: this,
                        exception: new FormatException(valueStateText),
                        id: this.getId(),
                        message: valueStateText,
                        property: "value",
                        newValue: valueToValidate
                    });
                } else {
                    const valueStateObj = this._validateValueInRange(valueToValidate);
                    valueState = valueStateObj.valueState;
                    valueStateText = valueStateObj.valueStateText;
                }

                this.setValueState(valueState);
                this.setValueStateText(valueStateText);
            },

            _onInputChange: function(event) {
                const enteredValue = event.getParameters().value;
                this._validateValue(enteredValue);

                this._updateSlidersValueViaValue(enteredValue);

                this.setProperty("value", enteredValue, true);

                this.fireEvent("change", {
                    value: this.getValue()
                });
            },

            _onSliderChange: function(event) {
                const params = event.getParameters();
                const value = params && params.value || 0;
                const intValue = parseInt(value, 10);
                const timeUnitCustomData = event.getSource().getCustomData().find((customData) => customData && customData.getKey() === "unit");
                const timeUnit = timeUnitCustomData && timeUnitCustomData.getValue();

                this._updateValueViaSliders(timeUnit.getIdentifier(), intValue);

                this.fireEvent("change", {
                    value: this.getValue()
                });
            },

            setEditable: function(editable) {
                const isEditable = !!editable;
                this.getAggregation("_input").setEditable(isEditable);
                this.getTimeUnitSliders().forEach((slider) => {
                    slider.setEnabled(isEditable);
                });
                this.setProperty("editable", isEditable, true);
            },

            setEnabled: function(enabled) {
                const isEnabled = !!enabled;
                this.getAggregation("_input").setEnabled(isEnabled);
                this.getTimeUnitSliders().forEach((slider) => {
                    slider.setEnabled(isEnabled);
                });

                this.setProperty("enabled", isEnabled, true);
            },

            setName: function(name) {
                this.getAggregation("_input").setName(name);
                this.setProperty("name", name, true);
            },

            setShowValueStateMessage: function(showValueStateMessage) {
                const doShowValueStateMessage = !!showValueStateMessage;
                this.getAggregation("_input").setShowValueStateMessage(doShowValueStateMessage);
                this.setProperty("showValueStateMessage", doShowValueStateMessage, true);
            },

            setTextAlign: function(textAlign) {
                this.getAggregation("_input").setTextAlign(textAlign);
                this.setProperty("textAlign", textAlign, true);
            },

            setTextDirection: function(textDirection) {
                this.getAggregation("_input").setTextDirection(textDirection);
                this.setProperty("textDirection", textDirection, true);
            },

            setWidth: function(width) {
                this.getAggregation("_input").setWidth(width);
                this.setProperty("width", width, true);
            },

            setFieldGroupIds: function(fieldGroupIds) {
                this.getAggregation("_input").setFieldGroupIds(fieldGroupIds);
                this.setProperty("fieldGroupIds", fieldGroupIds, true);
            },

            setBusyIndicatorSize: function(busyIndicatorSize) {
                this.getAggregation("_input").setBusyIndicatorSize(busyIndicatorSize);
                this.setProperty("busyIndicatorSize", busyIndicatorSize, true);
            },

            setBusyIndicatorDelay: function(busyIndicatorDelay) {
                this.getAggregation("_input").setBusyIndicatorDelay(busyIndicatorDelay);
                this.setProperty("busyIndicatorDelay", busyIndicatorDelay, true);
            },

            setBusy: function(busy) {
                const isBusy = !!busy;
                this.getAggregation("_input").setBusy(isBusy);
                this.setProperty("busy", isBusy, true);
            },

            setBlocked: function(blocked) {
                const isBlocked = !!blocked;
                this.getAggregation("_input").setBlocked(isBlocked);
                this.setProperty("blocked", isBlocked, true);
            },

            setVisible: function(visible) {
                const isVisible = !!visible;
                this.getAggregation("_input").setVisible(isVisible);
                this.getAggregation("_popover").setVisible(isVisible);
                this.setProperty("visible", isVisible, true);
            },

            setRequired: function(required) {
                const isRequired = !!required;
                this.getAggregation("_input").setRequired(isRequired);
                this.setProperty("required", isRequired, true);
            },

            getDomRef: function(suffix) {
                let domRef;
                const input = this.getAggregation("_input");
                const popover = this.getAggregation("_popover");

                if(input) {
                    domRef = input.getDomRef(suffix);
                } else if(popover && !domRef) {
                    domRef = popover.getDomRef(suffix);
                } else if(!domRef) {
                    // JS style super call
                    domRef = InputBase.prototype.getDomRef.call(this, suffix);
                }

                return domRef;
            },

            getFocusDomRef: function() {
                return this.getAggregation("_input").getFocusDomRef();
            },

            _getMaxInMillis: function() {
                return this._maxInMillis ? this._maxInMillis : undefined;
            },

            _getMinInMillis: function() {
                return this._minInMillis ? this._minInMillis : 0;
            },

            _getHighestRankIdentifier: function() {
                return this._highestRankIdentifier;
            },

            _isHighestRankUnit: function(unitIdentifier) {
                const highestRankIdentifier = this._getHighestRankIdentifier();
                return highestRankIdentifier && highestRankIdentifier === unitIdentifier;
            },

            _addUnit: function(timeUnit) {
                if(timeUnit && timeUnit.getIdentifier && this._timeUnitData && this._timeUnitData.hasOwnProperty(timeUnit.getIdentifier())) {
                    this._timeUnitData[timeUnit.getIdentifier()].unit = timeUnit;
                }
            },

            _getResourceBundle: function() {
                return this._resourceBundle;
            },

            addEventDelegate: function(delegate, thisReference) {
                const input = this.getAggregation("_input");

                if(input && delegate && delegate.hasOwnProperty("onkeypress")) {
                    input.addEventDelegate(delegate, thisReference);
                }

                // JS style super call
                return InputBase.prototype.addEventDelegate.call(this, delegate, thisReference);
            }
        });
    }
);