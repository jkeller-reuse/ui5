sap.ui.define([
    "sap/ui/base/Object"
],

    function (Object) {
        "use strict";

        /**
         * @param {typeof sap.ui.base.Object} Object
         */
        var TimeUnit = Object.extend("de.jkeller.reuse.control.duration.TimeUnit", {
            constructor: function(identifier, rank, toMillisFactor) {
                this._identifier = identifier;
                this._rank = rank;
                this._toMillisFactor = toMillisFactor;

                Object.apply(this, []);
            },

            /**
             * @public
             * @returns {@typeof string} the identifier of this time unit.
             */
            getIdentifier: function() {
                return this._identifier;
            },

            /**
             * @public
             * @returns {@typeof Number} rank of this time unit.
             */
            getRank: function() {
                return this._rank;
            },

            /**
             * @public
             * @param {@typeof Number} value 
             * @returns value converted into millis (from this time unit)
             */
            toMillis: function(value) {
                return BigInt(value) * BigInt(this._toMillisFactor);
            },

            /**
             * @public
             * @param {@typeof Number} millis 
             * @returns value parsed / converted into time unit from provided millis
             */
            parseMillis: function(millis) {
                return BigInt(millis) / BigInt(this._toMillisFactor);
            }
        });

        /**
         * @public
         * valid / available (time) unit identifiers
         */
        TimeUnit.Identifier = {
            Hours: "H",
            Minutes: "m",
            Seconds: "s",
            Millis: "S"
        }

        TimeUnit.Rank = {
            Hours: 0,
            Minutes: 1,
            Seconds: 2,
            Millis: 3
        }

        TimeUnit.IdentifierToRank = {
            "H": 0,
            "m": 1,
            "s": 2,
            "S": 3
        }

        const HOUR = new TimeUnit(TimeUnit.Identifier.Hours, TimeUnit.Rank.Hours, 60 * 60 * 1000);
        const MINUTE = new TimeUnit(TimeUnit.Identifier.Minutes, TimeUnit.Rank.Hours, 60 * 1000);
        const SECOND = new TimeUnit(TimeUnit.Identifier.Seconds, TimeUnit.Rank.Hours, 1000);
        const MILLI = new TimeUnit(TimeUnit.Identifier.Millis, TimeUnit.Rank.Hours, 1);

        /**
         * @public
         * @returns {@typeof de.jkeller.reuse.control.duration.TimeUnit} representing hours
         */
        TimeUnit.Hours = function() {
            return HOUR;
        }

        /**
         * @public
         * @returns {@typeof de.jkeller.reuse.control.duration.TimeUnit} representing minutes
         */
        TimeUnit.Minutes = function() {
            return MINUTE;
        }

        /**
         * @public
         * @returns {@typeof de.jkeller.reuse.control.duration.TimeUnit} representing seconds
         */
        TimeUnit.Seconds = function() {
            return SECOND;
        }

        /**
         * @public
         * @returns {@typeof de.jkeller.reuse.control.duration.TimeUnit} representing millis 
         * (seconds)
         */
        TimeUnit.Millis = function() {
            return MILLI;
        }

        /**
         * @public
         * @param {@typeof string} identifier 
         * @returns {@typeof de.jkeller.reuse.control.duration.TimeUnit} represented by provided
         * identifier
         */
        TimeUnit.get = function(identifier) {
            switch(identifier) {
                case TimeUnit.Identifier.Hours:
                    return TimeUnit.Hours();
                case TimeUnit.Identifier.Minutes:
                    return TimeUnit.Minutes();
                case TimeUnit.Identifier.Seconds:
                    return TimeUnit.Seconds();
                case TimeUnit.Identifier.Millis:
                    return TimeUnit.Millis();
            }
        }

        /**
         * @public
         * @param {@typeof string} identifier 
         * @returns {@typeof de.jkeller.reuse.control.duration.TimeUnit} rank of TimeUnit represented 
         * by provided identifier
         */
        TimeUnit.getRankForUnit = function(identifier) {
            return TimeUnit.IdentifierToRank.hasOwnProperty(identifier) ? TimeUnit.IdentifierToRank[identifier] : undefined;
        }

        /**
         * @public
         * @param {@typeof de.jkeller.reuse.control.duration.TimeUnit} identifier1 
         * @param {@typeof de.jkeller.reuse.control.duration.TimeUnit} identifier2 
         * @returns comparison result of identifer1 and identifier2 according to 
         * their rank. If identifier1 is higher than identifier2 a positive value 
         * is returned, and vice versa a negative one if identifier1 has lower 
         * rank than identifier2. If identifier1 and identifier2 are equals 0 is 
         * returned.
         */
        TimeUnit.compareRank = function(identifier1, identifier2) {
            const rank1 = TimeUnit.getRankForUnit(identifier1);
            const rank2 = TimeUnit.getRankForUnit(identifier2);

            return rank1 !== undefined && rank2 !== undefined ? rank1 - rank2 : undefined;
        }

        return TimeUnit;
    }
);