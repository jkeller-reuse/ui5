sap.ui.define([
    "sap/m/ResponsiveScale"
],

    /**
     * Provide a ticks only for the min and max value of this scale.
     * @param {@typeof sap.m.ResponsiveScale} ResponsiveScale 
     */
    function (ResponsiveScale) {
        "use strict";

        return ResponsiveScale.extend("de.jkeller.reuse.control.duration.MinMaxScale", {
            calcNumberOfTickmarks: function() {
                return 1;
            }
        });
    }
);