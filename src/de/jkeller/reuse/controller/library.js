sap.ui.define(
    function() {
        "use strict";

        var library = sap.ui.getCore().initLibrary({
            name: "de.jkeller.reuse.controller",
            version: "0.1-SNAPSHOT",
            dependencies: [
                "sap.ui.core"
            ],
            interfaces: [
                "de.jkeller.reuse.controller.BaseController",
                "de.jkeller.reuse.controller.RoutePatternExtension",
                "de.jkeller.reuse.controller.TablePopinExtension"
            ]
        });	
    
        return library; 
    }
);