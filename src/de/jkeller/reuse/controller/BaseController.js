sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History",
    "sap/ui/core/UIComponent",
    "sap/ui/core/mvc/OverrideExecution",
    "de/jkeller/reuse/controller/RoutePatternExtension",
    "de/jkeller/reuse/controller/TablePopinExtension"
],
    /**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     * @param {typeof sap.ui.core.routing.History} History
     * @param {typeof sap.ui.core.UIComponent} UIComponent
     * @param {typeof sap.ui.core.mvc.OverrideExecution} OverrideExecution
     * @param {typeof de.jkeller.reuse.controller.RoutePatternExtension} RoutePatternExtension
     * @param {typeof de.jkeller.reuse.controller.TablePopinExtension} TablePopinExtension
     */
    function (Controller, History, UIComponent, OverrideExecution, RoutePatternExtension, TablePopinExtension) {
        "use strict";

        const ON_VIEW_ROUTE_PATTERN_MATCHED_EVENT_NAME = "de.jkeller.reuse.route.pattern.extension-onViewRoutePatternMatched";

        return Controller.extend("de.jkeller.reuse.controller.BaseController", {
            metadata: {
                methods: {
                    onShowAllColumns: {
                        public: true,
                        final: true,
                        overrideExecution: OverrideExecution.Instead
                    },
                    onTablePopinChanged: {
                        public: true,
                        final: true,
                        overrideExecution: OverrideExecution.Instead
                    },
                    onTableAllColumnsToggled: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    getViewRoutes: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    getModelNamesToLoadOnRouteMatched: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    onViewRoutePatternMatched: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    getViewRoutePatternMatchedEventName: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    }
                }
            },

            routePatternExtension: RoutePatternExtension.override({
                getViewRoutes: function() {
                    return this.base.getViewRoutes();
                }, 

                getModelNamesToLoadOnRouteMatched: function() {
                    return this.base.getModelNamesToLoadOnRouteMatched();
                },

                onViewRoutePatternMatched: function(event) {
                    return this.base.fireViewRoutePatternMatched(event.getParameters(), event.getSource());
                }
            }),

            tablePopinExtension: TablePopinExtension.override({
                onShowAllColumns: function(event) {
                    this.base.onTableAllColumnsToggled(event)
                }
            }),

            /**
             * Create a new BaseController instance.
             * @public
             * @param {string | object[]} name The name of the controller to instantiate. If a controller is defined as real sub-class, 
             *      the "arguments" of the sub-class constructor should be given instead.
             * @param {string} homeRoute route to navigate to if there is no previous route in the history.
             * @param {sap.ui.core.routing.Router} router used for navigation. If not provided the (default) router via UIComponent is 
             *      used.
             */
            constructor: function (name, homeRoute, router) {
                Controller.apply(this, [name]);
                this._homeRoute = homeRoute;
                this._router = router && router instanceof sap.ui.core.routing.Router ? router 
                    : undefined;
            },

            onInit: function() {
                if(!this._router) {
                    this.setRouter(UIComponent.getRouterFor(this));
                }
            },

            /**
             * @returns the router instance
             */
            getRouter: function () {
                return this._router;
            },

            /**
             * @param {sap.ui.core.routing.Router} router used for navigation. If not provided the (default) router via UIComponent is 
             *      used. 
             */
            setRouter: function(router) {
                this._router = router;
            },

            /**
             * @returns {string} route that is declared as home, used to navigate to if there is no other history to navigate for back navigation.
             */
            getHomeRoute: function() {
                return this._homeRoute;
            },

            /**
             * Logic for back navigation, if no hash for back navigation is defined it will navigate to initial page (target "")
             */
            onNavBack: function() {
                const prevHash = History.getInstance().getPreviousHash();

                if(prevHash === undefined && this._homeRoute) {
                    this.getRouter().navTo(this._homeRoute);
                } else {
                    window.history.go(-1);
                }
            },

            /**
             * Called after the column display has been toggled.
             * @param {typeof sap.ui.base.Event} event on button press.
             */
            onTableAllColumnsToggled: function(event) {
            },

            /**
             * Toggles to show or hide all columns of a table on button press event. The button of this event 
             * handler has to be assigned to a table, i.e., a table must be reached via parent relationships.
             * @param {typeof sap.ui.base.Event} event on button press.
             */
            onShowAllColumns: function(event) {
                this.tablePopinExtension.onShowAllColumns(event);
            },

            /**
             * Set visibility of the show more button (identified by given buttonId) of a table, if no buttonId is 
             * passed nothing can be hidden / shown.
             * @param {*} event on table popin changed
             * @param {*} buttonId of button toggling to show or hide all columns of a table (see also function onShowAllColumns)
             */
            onTablePopinChanged: function(event, buttonId) {
                this.tablePopinExtension.onTablePopinChanged(event, buttonId);
            },

            /**
             * Get the route(s) of the current view to be able to execute actions on page open 
             * (matching of route's view). Either return a single string or an array of strings.
             * @public
             * @returns {typeof string | typeof Array}
             */
            getViewRoutes: function() {
            },

            /**
             * Get the keys of models to be loaded on page open. 'load' of model keys returned 
             * by this method will be triggered on each page opening (route matched). Either 
             * return an Object mapping route names to model keys that shall be loaded or an 
             * array of strings (model keys).
             * @public
             * @returns {typeof Array | typeof Object} of model keys to be loaded on page opening
             */
            getModelNamesToLoadOnRouteMatched: function() {
                return [];
            },

            /**
             * To be called once the route pattern of this view is matched.
             * @public
             * @param {typeof sap.ui.base.Event} event 
             */
            onViewRoutePatternMatched: function(event) {
            },

            /**
             * @public
             * @returns {typeof string} name of event that is used when route pattern is matched.
             */
            getViewRoutePatternMatchedEventName: function() {
                return ON_VIEW_ROUTE_PATTERN_MATCHED_EVENT_NAME;
            },

            /**
             * Fires the onViewRoutePatternMatched event
             * @param {typeof JSON} parameters to be passed along the event
             * @param {typeof any} source object of the event
             */
            fireViewRoutePatternMatched: function(parameters, source) {
                this.onViewRoutePatternMatched(new sap.ui.base.Event(this.getViewRoutePatternMatchedEventName(), source, parameters));
            }
        });
    }
);