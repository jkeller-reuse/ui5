sap.ui.define([
    "sap/ui/core/mvc/ControllerExtension",
    "sap/ui/core/mvc/OverrideExecution",
    "sap/ui/core/UIComponent"
], 
    function(ControllerExtension, OverrideExecution, UIComponent) {
        "use strict";

        return ControllerExtension.extend("de.jkeller.reuse.controller.RoutePatternExtension", {
            metadata: {
                methods: {
                    getViewRoutes: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    getModelNamesToLoadOnRouteMatched: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    onViewRoutePatternMatched: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.After
                    }
                }
            },

            override: {
                onInit: function() {
                    return this._onInit();
                }
            },

            _onInit: function() {
                const viewRoutes = this._getViewRoutes();
                if(viewRoutes) {
                    viewRoutes.forEach((viewRoute) => {
                        const route = this._getRouter().getRoute(viewRoute);
                        if(route) {
                            route.attachPatternMatched(this.onViewRoutePatternMatched, this);
                        }
                    });
                }
            },

            _getViewRoutes: function() {
                const viewRoutes = this.getViewRoutes();
                return viewRoutes && (viewRoutes instanceof Array ? viewRoutes : [viewRoutes]) || []
            },

            _getModelNamesToLoadOnRouteMatched: function() {
                const modelNames = this.getModelNamesToLoadOnRouteMatched();
                if(modelNames instanceof Array) {
                    const routeToModelNames = {};
                    this._getViewRoutes().forEach((route) => {
                        routeToModelNames[route] = modelNames;
                    });

                    return routeToModelNames;
                }

                return modelNames;

            },

            /**
             * @returns the router instance
             */
            _getRouter : function () {
                return UIComponent.getRouterFor(this.getView().getController());
            },

            /**
             * Get the route(s) of the current view to be able to execute actions on page open 
             * (matching of route's view). Either return a single string or an array of strings.
             * @public
             * @returns {typeof string | typeof Array}
             */
            getViewRoutes: function() {
            },

            /**
             * Get the keys of models to be loaded on page open. 'load' of model keys returned 
             * by this method will be triggered on each page opening (route matched). Either 
             * return an Object mapping route names to model keys that shall be loaded or an 
             * array of strings (model keys).
             * @public
             * @returns {typeof Array | typeof Object} of model keys to be loaded on page opening
             */
            getModelNamesToLoadOnRouteMatched: function() {
                return [];
            },

            /**
             * To be called once the route pattern of this view is matched.
             * @public
             * @param {typeof sap.ui.base.Event} event 
             */
            onViewRoutePatternMatched: function(event) {
                const routeToModelNames = this._getModelNamesToLoadOnRouteMatched();
                if(routeToModelNames && routeToModelNames instanceof Object) {
                    const routeName = event && event.getParameter("name");

                    if(routeName && routeToModelNames.hasOwnProperty(routeName)) {
                        const modelNames = routeToModelNames[routeName];
                        if(modelNames instanceof Array) {
                            modelNames.forEach((modelName) => {
                                const model = this.getView().getModel(modelName);
                                if(model && model.load && typeof model.load === "function") {
                                    model.load();
                                }
                            });
                        }
                    }
                }
            }
        });
    }
);