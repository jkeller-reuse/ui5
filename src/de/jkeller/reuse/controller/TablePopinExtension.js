sap.ui.define([
    "sap/ui/core/mvc/ControllerExtension",
    "sap/ui/core/mvc/OverrideExecution"
], 
    function(ControllerExtension, OverrideExecution) {
        const SHOW_MORE_INDICATOR_PREFIX = "_tableShowMore--";
        const TABLE_PREV_HIDDEN_IN_POPIN_PREFIX = "_tablePrevHiddenInPopinPrios--";

        return ControllerExtension.extend("de.jkeller.reuse.controller.TablePopinExtension", {
            metadata: {
                methods: {
                    onShowAllColumns: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.After
                    },
                    onTablePopinChanged: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Before
                    },
                    getShowMoreIndicatorPrefix: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    },
                    getTablePreviouslyHiddenInPopinPrefix: {
                        public: true,
                        final: false,
                        overrideExecution: OverrideExecution.Instead
                    }
                }
            },

            /**
             * Toggles to show or hide all columns of a table on button press event. The button of this event 
             * handler has to be assigned to a table, i.e., a table must be reached via parent relationships.
             * @param {typeof sap.ui.base.Event} event on button press.
             */
            onShowAllColumns: function(event) {
                let moreButton = event.getSource();
                let table = this._getParentTable(moreButton);
                let hiddenInPopinPrios = !this._isShowMore4Table(table)? []: this._getPrevHiddenInPopinPrios4Table(table);
                
                if(table) {
                    this._setPrevHiddenInPopinPrios4Table(table, table.getHiddenInPopin());
                    this._setShowMoreIndicator4Table(table, !this._isShowMore4Table(table));
                    table.setHiddenInPopin(hiddenInPopinPrios);

                    moreButton.setType(this._isShowMore4Table(table)? sap.m.ButtonType.Emphasized: sap.m.ButtonType.Transparent);
                }
            },

            /**
             * Set visibility of the show more button (identified by given buttonId) of a table, if no buttonId is 
             * passed nothing can be hidden / shown.
             * @param {*} event on table popin changed
             * @param {*} buttonId of button toggling to show or hide all columns of a table (see also function onShowAllColumns)
             */
            onTablePopinChanged: function(event, buttonId) {
                let moreButton = this.getView().byId(buttonId);

                if(moreButton) {
                    moreButton.setVisible(event.getSource().getColumns().some(function(column) {
                        return !column.getVisible() || column.isPopin();
                    }));
                }
            },

            getShowMoreIndicatorPrefix: function() {
                return SHOW_MORE_INDICATOR_PREFIX;
            },

            getTablePreviouslyHiddenInPopinPrefix: function() {
                return TABLE_PREV_HIDDEN_IN_POPIN_PREFIX;
            },

            /**
             * Try to receive a table from a control via parent relationships.
             * @param {*} control 
             * @returns the sap.m.Table object that was received from parent relation, undefined if no table object 
             *          could be found.
             */
            _getParentTable: function(control) {
                if(control && control.getParent && control.getParent()) {
                    if(control.getParent() instanceof sap.m.Table) {
                        return control.getParent();
                    }

                    return this._getParentTable(control.getParent());
                } 

                return;
            },

            /**
             * Receive the string that is used to access the showMore indicator for given table.
             * @param {*} table 
             * @returns 
             */
            _getShowMoreIndicatorKey4Table: function(table) {
                return table && table.getId? `${this.getShowMoreIndicatorPrefix()}${table.getId()}`: "";
            },

            /**
             * Receive the string that is used to access the list of previous hiddenInPoppin property of 
             * given table
             * @param {*} table 
             * @returns 
             */
            _getPrevHiddenInPopinKey4Table: function(table) {
                return table && table.getId? `${this.getTablePreviouslyHiddenInPopinPrefix()}${table.getId()}`: "";
            },

            /**
             * Receive the stored hiddenInPopin property of given table.
             * @param {*} table 
             * @returns stored hiddenInPopin property of given table or empty array if hiddenInPopin property was not 
             *          stored yet.
             */
            _getPrevHiddenInPopinPrios4Table(table) {
                let prevHiddenInPopinPriosKey = this._getPrevHiddenInPopinKey4Table(table);

                if(prevHiddenInPopinPriosKey && this.hasOwnProperty(prevHiddenInPopinPriosKey) && this[prevHiddenInPopinPriosKey]) {
                    return this[prevHiddenInPopinPriosKey];
                }

                return [];
            },

            _setPrevHiddenInPopinPrios4Table(table, hiddenInPopinPrios) {
                let prevHiddenInPopinPriosKey = this._getPrevHiddenInPopinKey4Table(table);

                if(prevHiddenInPopinPriosKey) {
                    this[prevHiddenInPopinPriosKey] = hiddenInPopinPrios? hiddenInPopinPrios: [];
                }
            },

            _isShowMore4Table(table) {
                let showMoreKey = this._getShowMoreIndicatorKey4Table(table);
                return this.hasOwnProperty(showMoreKey) && this[showMoreKey];
            },

            _setShowMoreIndicator4Table(table, showMore) {
                let showMoreKey = this._getShowMoreIndicatorKey4Table(table);

                if(showMoreKey) {
                    this[showMoreKey] = !!showMore;
                }
            }
        });
    }
);