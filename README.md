# General Information


## BaseCtroller

de.jkeller.reuse.controller.Basecontroller

Enables automatic loading of `json` data bound to diverse models (`JSONModelExtended`) of current view once the view is opened. To accomplish this the route matched of `Router` is used. 
Possibility to show / hide detailed information (depending on screen size) for tables by use of `column importance` and `hidden in popin`.


## JSONControllerExtended

de.jkeller.reuse.model.JSONModelExtended

Provide extended behavior for an easier `TwoWay` fashioned usage of base `sap.ui.model.json.JSONModel`. In particular data stored in `JSONModelExtended` can easily be sent in `POST` to the backend.


## DurationSlider

de.jkeller.reuse.control.duration.DurationSlider

Provides a control allowing the user to input time durations that must comply with a defined format. In contrast to UI5 TimePickerSlider the `DurationSlider` enabled input of milliseconds as well. Allowed time format identifiers are:
* `H`: hour(s)
* `m`: minute(s)
* `s`: second(s)
* `S`: millisecond(s)


## BasePage

de.jkeller.reuse.test.opa.page.BasePage

Diverse `assertions` and `actions` for common usage in `OPA5` tests.


# Usage

First clone this repo and change to the root directory of the repository (direcotry containing the `.git` folder).


## Dependency via NPM

To use this library you can consume it via [`npm link`](https://docs.npmjs.com/cli/v9/commands/npm-link). For usage  via `npm`, after cloning, install, build and link it:
```bash
npm install
npm run build
npm link
```
This will create a symlink of the repository clone to `/usr/local/lib/node_modules/de.jkeller.reuse` (Linux).
(By default build of the library is already containing required dependencies (self contained) [--all](https://sap.github.io/ui5-tooling/stable/pages/CLI/#ui5-build). If you wish to provide `OpenUI5` dependencies globally on your server only once you may want to change this such that required dependencies are not fixed).

Then in the project where you want use the library simply run
```bash
npm link de.jkeller.reuse
```

**NOTE:** Alternatively to `npm` you can also use  [`yarn`](https://classic.yarnpkg.com/en/docs/install#debian-stable) 1.x as mentioned in [`OpenUI5`](https://sap.github.io/ui5-tooling/stable/pages/OpenUI5/#linking-framework-dependencies) )working with local dependencies), or detailed in [OpenUI5 sample app](https://github.com/SAP/openui5-sample-app#working-with-local-dependencies).


## Dependency Definition

In your project's `package.json` add dependency to `de.jkeller.reuse`
```json
"dependencies": {
    "de.jkeller.reuse": "0.0.x"
}

...

"ui5": {
    "dependencies": [
        "de.jkeller.reuse"
    ]
}
```

Finally in `ui5.yaml` of your project append a `shim` to include the library (as mentioned in [UI5 Tooling](https://sap.github.io/ui5-tooling/stable/pages/extensibility/ProjectShims/#example-a))
```yaml
--- # new yaml document (append further configuration)

specVersion: '2.5'
kind: extension
type: project-shim
metadata:
  name: libs
shims:
  configurations:
    de-jkeller-reuse:
      specVersion: '2.5'
      type: library
      metadata:
        name: de-jkeller-reuse
      resources:
        configuration:
          paths:
            /resources/de/jkeller/reuse/model/: './node_modules/de.jkeller.reuse/dist/resources/de/jkeller/reuse/model'
```


## Dockerfile
For usage in a `Docker` you can build an image using included `Dockerfile` in root directory of this repository. The Docker image includes linked `de.jkeller.reuse` library via `npm`. To build the image simply run
```bash
docker build . --tag de.jkeller.reuse:v0.1
```
This image is based on [`node` image](https://hub.docker.com/_/node).

Finally you can use created image in your `Dockerfile` like
```Dockerfile
# syntax=docker/dockerfile:1
FROM de.jkeller.reuse:v0.1
# copy your sources
# change to work dir of consuming app
RUN npm link de.jkeller.reuse
# build your app
```
