module.exports = function(config) {
    "use strict";

    let chromeFlags = [
        "--window-size=1280,1024"
    ];
    
    let chromeFlagsNoSandbox = [
        "--no-sandbox"
    ];
    chromeFlags.forEach(function(entry) {
        chromeFlagsNoSandbox.push(entry);
    });

    config.set({
        frameworks: ["ui5"],
        browserConsoleLogOptions: {
            level: "error"
        },
        customLaunchers: {
            CustomChrome: {
                base: "Chrome",
                flags: chromeFlags
            },
            CustomChromeHeadless: {
                base: "ChromeHeadless",
                flags: chromeFlags
            },
            CustomChromeHeadlessNoSandbox: {
                base: "ChromeHeadless",
                flags: chromeFlagsNoSandbox
            }
        },
        reporters: ["progress", "coverage"],
        preprocessors: {
            "src/**/!(library|BasePage).js" : ["coverage"]
        },
        coverageReporter: {
            includeAllSources: true,
            reporters: [
                {
                    type: "html",
                    dir: "coverage",
                    subdir: "html"
                },
                {
                    type: "text"
                },
                {
                    type: "cobertura",
                    dir: "coverage",
                    subdir: "cobertura",
                    file: "cobertura.xml"
                }
            ],
        },
        browsers: ["CustomChromeHeadlessNoSandbox"],
        singleRun: true
    });
};
