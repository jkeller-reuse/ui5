window.suite = function() {
    "use strict";

    let oSuite = new parent.jsUnitTestSuite();
    let sContextPath = location.pathname.substring(0, location.pathname.lastIndexOf("/") + 1);

    oSuite.addTestPage(sContextPath + "../model/unit/unitTests.qunit.html");
    oSuite.addTestPage(sContextPath + "../controller/unit/unitTests.qunit.html");
    oSuite.addTestPage(sContextPath + "../controller/integration/opaTests.qunit.html");
    oSuite.addTestPage(sContextPath + "../validator/integration/opaTests.qunit.html");
    oSuite.addTestPage(sContextPath + "../control/unit/unitTests.qunit.html");
    oSuite.addTestPage(sContextPath + "../control/integration/opaTests.qunit.html");

    return oSuite;
};
