sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/opaQunit",
    "de/jkeller/reuse/controller/integration/arrangements/Startup",
    "de/jkeller/reuse/controller/integration/pages/MainPage",
    "sap/m/library"
], function(Opa5, opaTest, Startup) {
    "use strict";

    Opa5.extendConfig({
        arrangements: new Startup(),
        pollingInterval: 1
    });

    const TABLE_ID = "sample-table";
    const SHOW_DETAILS_BUTTON_ID = "sample-list-all-columns";

    QUnit.module("Small Screen - Table Popin");

    opaTest("Start app on small screen should see table show details button and no columns with importance low", function (Given, When, Then) {
        Given.iStartMyApp({width: 480, height: 270});

        Then.onTheMainPage.iShouldSeeOverflowToolbarButtonWithId(SHOW_DETAILS_BUTTON_ID, sap.m.ButtonType.Transparent)
            .and.iShouldNotSeeTableColumnWithLabel(TABLE_ID, "Column Low")
            .and.iShouldNotSeeTableColumnWithLabel(TABLE_ID, "Column Low 2");
    });

    opaTest("Click show details button, then button is emphasized and should see all columns", function(Given, When, Then) {
        When.onTheMainPage.iClickOverflowToolbarButtonWithId(SHOW_DETAILS_BUTTON_ID);

        Then.onTheMainPage.iShouldSeeOverflowToolbarButtonWithId(SHOW_DETAILS_BUTTON_ID, sap.m.ButtonType.Emphasized)
            .and.iShouldSeeTableColumnWithLabel(TABLE_ID, "Column Low")
            .and.iShouldSeeTableColumnWithLabel(TABLE_ID, "Column Low 2");
    });

    opaTest("Click show details button when details already displayed, then button is not emphasized and not all columns are shown", 
        function(Given, When, Then) {
        When.onTheMainPage.iClickOverflowToolbarButtonWithId(SHOW_DETAILS_BUTTON_ID);

        Then.onTheMainPage.iShouldSeeOverflowToolbarButtonWithId(SHOW_DETAILS_BUTTON_ID, sap.m.ButtonType.Transparent)
            .and.iShouldNotSeeTableColumnWithLabel(TABLE_ID, "Column Low")
            .and.iShouldNotSeeTableColumnWithLabel(TABLE_ID, "Column Low 2");

        Then.iTeardownMyAppFrame();
    });

    QUnit.module("Casual Screen Size");

    opaTest("Start app on big screen should not see table show details button and all columns visible", function (Given, When, Then) {
        Given.iStartMyApp();

        Then.onTheMainPage.iShouldNotSeeOverflowToolbarButtonWithId(SHOW_DETAILS_BUTTON_ID, sap.m.ButtonType.Transparent)
            .and.iShouldSeeTableColumnWithLabel(TABLE_ID, "Column Low")
            .and.iShouldSeeTableColumnWithLabel(TABLE_ID, "Column Low 2");

        Then.iTeardownMyAppFrame();
    });
});
