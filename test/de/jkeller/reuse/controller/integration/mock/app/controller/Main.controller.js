sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "de/jkeller/reuse/controller/BaseController"
],
    function (JSONModel, BaseController) {
        "use strict";

        return BaseController.extend("de.jkeller.reuse.controller.integration.mock.app.controller.Main", {
            onInit: function () {
                let sampleModel = new JSONModel([
                    {column1: "Some Value", column2: "Further Value", column3: "Awesome", column4: "Value", column5: "some", column6: "entry"},
                    {column1: "Second value", column2: "second entry", column3: "casual", column4: "val", column5: "further", column6: "record"}
                ]);

                this.getView().setModel(sampleModel, "sampleModel");
            },
        });
    }
);
