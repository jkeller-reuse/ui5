sap.ui.define([
    "sap/ui/test/Opa5",
    "de/jkeller/reuse/test/opa/page/BasePage"
], function (Opa5, BasePage) {
    "use strict";

    const VIEW_NAME = "app.view.Main";

    let basePage = new BasePage(VIEW_NAME);

    Opa5.createPageObjects({
        onTheMainPage: {
            actions: {
                ... basePage.actions
            },

            assertions: {
                ...basePage.assertions
            }
        }
    });
});