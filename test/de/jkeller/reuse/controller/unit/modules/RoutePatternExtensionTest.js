sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "de/jkeller/reuse/controller/RoutePatternExtension",
    "sap/ui/core/UIComponent",
    "sap/ui/base/Event"
], function(Controller, RoutePatternExtension, UIComponent, Event) {
    "use strict";

    const TEST_ROUTE = "testRoute";
    const TEST_MODEL = "testModel";
    const CONTROLLER = "controller";
    const TEST_OTHER_ROUTE = "someOtherRoute";
    const TEST_OTHER_MODEL = "someOtherModel";
    const TEST_ROUTE_MATCHED_EVENT = new Event("", {}, {name: TEST_ROUTE});


    QUnit.module("Test RoutePatternExtension", {
        before: function() {
            this.fakeRouter = {
                getRoute: function(routeName) {
                    let that = this;

                    this._lastRoute = routeName;

                    return {
                        attachPatternMatched: function(callback, listener) {
                            that._onPatternMatchedCallback = callback;
                            that._onPatternMatchedListener = listener;
                        }
                    }
                },

                getLastReceivedRoute: function() {
                    return this._lastRoute;
                },

                getOnPatternMatchedCallback: function() {
                    return this._onPatternMatchedCallback;
                },

                getOnPatternMatchedListener: function() {
                    return this._onPatternMatchedListener;
                },

                clear: function() {
                    this._lastRoute = undefined;
                    this._onPatternMatchedCallback = undefined;
                    this._onPatternMatchedListener = undefined;
                }
            };

            this.fakeView = {
                _isModelLoaded: {},
                getModel: function(modelName) {
                    let that = this;

                    return {
                        load: function() {
                            that._isModelLoaded[modelName] = true
                        }
                    }
                },

                isModelLoaded: function(modelName) {
                    if(modelName) {
                        const returnValue = this._isModelLoaded.hasOwnProperty(modelName) && this._isModelLoaded[modelName];
                        return returnValue;
                    }

                    const isModelLoadedValues = Object.values(this._isModelLoaded);
                    return isModelLoadedValues && isModelLoadedValues.length > 0 && isModelLoadedValues[0];
                },

                getController: function() {
                    return CONTROLLER;
                },

                clear: function() {
                    this._isModelLoaded = {};
                }
            };
        },

        after: function() {
            this.fakeRouter.clear();
        },

        beforeEach: function() {
            this.sinonSandbox = sinon.sandbox.create();
        },

        afterEach: function() {
            this.sinonSandbox.restore();
            this.fakeView.clear();
        }
    });

    QUnit.test("Default getViewRoute", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        assert.equal(routePatternExtension.getViewRoutes(), undefined);
    });

    QUnit.test("Default getModelNamesToLoadOnRouteMatched", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        let act = routePatternExtension.getModelNamesToLoadOnRouteMatched();

        assert.notEqual(act, undefined);
        assert.equal(act.length, 0);
    });

    QUnit.test("Test onInit", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getViewRoutes").returns(TEST_ROUTE);
        this.sinonSandbox.stub(routePatternExtension, "_getRouter").returns(this.fakeRouter);

        routePatternExtension._onInit();

        assert.equal(this.fakeRouter.getLastReceivedRoute(), TEST_ROUTE);
        assert.equal(this.fakeRouter.getOnPatternMatchedListener(), routePatternExtension);
        assert.equal(this.fakeRouter.getOnPatternMatchedCallback(), routePatternExtension.onViewRoutePatternMatched);
    });

    QUnit.test("Test onViewRoutePatternMatched: model names undefined", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns(undefined);

        routePatternExtension.onViewRoutePatternMatched();

        assert.notOk(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: model empty JSON", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getViewRoutes").returns(TEST_ROUTE);
        this.sinonSandbox.stub(routePatternExtension, "getView").returns(this.fakeView);
        this.sinonSandbox.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns({});

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.notOk(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: model names empty string", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getViewRoutes").returns(TEST_ROUTE);
        this.sinonSandbox.stub(routePatternExtension, "getView").returns(this.fakeView);
        this.sinonSandbox.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns("");

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.notOk(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: model empty array", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getViewRoutes").returns(TEST_ROUTE);
        this.sinonSandbox.stub(routePatternExtension, "getView").returns(this.fakeView);
        this.sinonSandbox.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns([]);

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.notOk(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: load not a function", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getView").returns({
            getModel: function() {
                return {
                    load: {
                    }
                };
            }
        });
        this.sinonSandbox.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns([TEST_MODEL]);

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.notOk(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: model loaded", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        this.sinonSandbox.stub(routePatternExtension, "getViewRoutes").returns(TEST_ROUTE);
        this.sinonSandbox.stub(routePatternExtension, "getView").returns(this.fakeView);
        this.sinonSandbox.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns([TEST_MODEL]);

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.ok(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: multiple routes with simple model array containing one model, expect models loaded", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        sinon.stub(routePatternExtension, "getViewRoutes").returns([TEST_ROUTE, TEST_OTHER_ROUTE]);
        sinon.stub(routePatternExtension, "getView").returns(this.fakeView);
        sinon.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns([TEST_MODEL]);

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.ok(this.fakeView.isModelLoaded());
    });

    QUnit.test("Test onViewRoutePatternMatched: multiple routes with simple model array containing multiple models, expect models loaded", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        sinon.stub(routePatternExtension, "getViewRoutes").returns([TEST_ROUTE, TEST_OTHER_ROUTE]);
        sinon.stub(routePatternExtension, "getView").returns(this.fakeView);
        sinon.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns([TEST_MODEL, TEST_OTHER_MODEL]);

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.ok(this.fakeView.isModelLoaded(TEST_MODEL));
        assert.ok(this.fakeView.isModelLoaded(TEST_OTHER_MODEL));
    });

    QUnit.test("Test onViewRoutePatternMatched: multiple routes with dedicated model mapping, expect matching model loaded", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        sinon.stub(routePatternExtension, "getViewRoutes").returns([TEST_ROUTE, TEST_OTHER_ROUTE]);
        sinon.stub(routePatternExtension, "getView").returns(this.fakeView);
        let routeToModel = {};
        routeToModel[TEST_ROUTE] = [TEST_MODEL];
        routeToModel[TEST_OTHER_ROUTE] = [TEST_OTHER_MODEL];
        sinon.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns(routeToModel);

        routePatternExtension.onViewRoutePatternMatched(TEST_ROUTE_MATCHED_EVENT);

        assert.ok(this.fakeView.isModelLoaded(TEST_MODEL));
        assert.notOk(this.fakeView.isModelLoaded(TEST_OTHER_MODEL));
    });

    QUnit.test("Test onViewRoutePatternMatched: event undefined", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        sinon.stub(routePatternExtension, "getViewRoutes").returns(TEST_ROUTE);
        sinon.stub(routePatternExtension, "getView").returns(this.fakeView);
        sinon.stub(routePatternExtension, "getModelNamesToLoadOnRouteMatched").returns([]);

        routePatternExtension.onViewRoutePatternMatched();

        assert.notOk(this.fakeView.isModelLoaded());
    });

    QUnit.test("_getRouter requests UIComponent router", function(assert) {
        let routePatternExtension = new RoutePatternExtension();

        let getRouterForStub = this.sinonSandbox.stub(UIComponent, "getRouterFor").returns({});
        this.sinonSandbox.stub(routePatternExtension, "getView").returns(this.fakeView);

        let router = routePatternExtension._getRouter();

        assert.notEqual(router, undefined);
        assert.notEqual(getRouterForStub.getCall(0), undefined);
        //assert.equal(getRouteForCall.args[0], CONTROLLER);
    });
});