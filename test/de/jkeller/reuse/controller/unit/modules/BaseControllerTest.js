sap.ui.define([
    "de/jkeller/reuse/controller/BaseController",
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/Router",
    "sap/ui/core/UIComponent",
    "sap/ui/core/routing/History"
], function(BaseController, Controller, Router, UIComponent, History) {
    "use strict";

    const HOME_ROUTE = "testRoute";
    const CONTROLLER_NAME = "unitTest.name";

    let FakeRouter = Router.extend("de.jkeller.unitTest.fake.router", {
        getRoute: function(routeName) {
            let that = this;

            this._lastRoute = routeName;

            return {
                attachPatternMatched: function(callback, listener) {
                    that._onPatternMatchedCallback = callback;
                    that._onPatternMatchedListener = listener;
                }
            }
        },

        getLastReceivedRoute: function() {
            return this._lastRoute;
        },

        getOnPatternMatchedCallback: function() {
            return this._onPatternMatchedCallback;
        },

        getOnPatternMatchedListener: function() {
            return this._onPatternMatchedListener;
        },

        navTo: function(target) {
            this._lastNavTo = target;
        },

        getLastNavTo: function() {
            return this._lastNavTo;
        },

        clear: function() {
            this._lastRoute = undefined;
            this._onPatternMatchedCallback = undefined;
            this._onPatternMatchedListener = undefined;
            this._lastNavTo = undefined;
        }
    });

    QUnit.module("Test BaseController", {
        before: function() {
            this.fakeRouter = new FakeRouter();

            this.fakeView = {
                getModel: function(modelName) {
                    let that = this;

                    return {
                        load: function() {
                            that._isModelLoaded = true
                        }
                    }
                },

                isModelLoaded: function() {
                    return this._isModelLoaded;
                },

                clear: function() {
                    this._isModelLoaded = false;
                }
            };
        },

        after: function() {
            this.fakeRouter.clear();
            this.fakeView.clear();
        },

        beforeEach: function() {
            this.sinonSandbox = sinon.sandbox.create();
        },

        afterEach: function() {
            this.sinonSandbox.restore();
        }
    });

    QUnit.test("Constructor with provided home route and router", function(assert) {
        let baseController = new BaseController(CONTROLLER_NAME, HOME_ROUTE, this.fakeRouter);
        
        assert.equal(baseController.getHomeRoute(), HOME_ROUTE);
        assert.equal(baseController.getRouter(), this.fakeRouter);
    });

    QUnit.test("Constructor w/o router uses UIComponent router after onInit", function(assert) {
        this.sinonSandbox.stub(UIComponent, "getRouterFor").returns(this.fakeRouter);

        let baseController = new BaseController(CONTROLLER_NAME, HOME_ROUTE);

        assert.equal(baseController.getRouter(), undefined);

        baseController.onInit();

        assert.equal(baseController.getRouter(), this.fakeRouter);
    });

    QUnit.test("Check tablePopin extension is called onShowAllColumns", function(assert) {
        let finished = assert.async();

        Controller.create({
            name: "de.jkeller.reuse.controller.unit.mock.TestController"
        }).then((controller) => {
            let showAllColumnsStub = this.sinonSandbox.stub(controller.tablePopinExtension, "onShowAllColumns");

            controller.onShowAllColumns();

            assert.notEqual(showAllColumnsStub.getCall(0), undefined);
            finished();
        });
    });

    QUnit.test("Check tablePopin extension is called onTablePopinChanged", function(assert) {
        let finished = assert.async();

        Controller.create({
            name: "de.jkeller.reuse.controller.unit.mock.TestController"
        }).then((controller) => {
            let onTablePopinChangedStub = this.sinonSandbox.stub(controller.tablePopinExtension, "onTablePopinChanged");

            controller.onTablePopinChanged();

            assert.notEqual(onTablePopinChangedStub.getCall(0), undefined);
            finished();
        });
    });

    QUnit.test("NavBack navigates to home route if there is no history", function(assert) {
        let fakeHistory = {
            getPreviousHash: function() {
                return undefined;
            }
        }

        this.sinonSandbox.stub(History, "getInstance").returns(fakeHistory);

        let baseController = new BaseController(CONTROLLER_NAME, HOME_ROUTE, this.fakeRouter);

        assert.equal(this.fakeRouter.getLastNavTo(), undefined);
        assert.equal(baseController.getHomeRoute(), HOME_ROUTE);

        baseController.onNavBack();

        assert.equal(this.fakeRouter.getLastNavTo(), baseController.getHomeRoute());
    });
});