QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "de/jkeller/reuse/controller/unit/modules/RoutePatternExtensionTest",
        "de/jkeller/reuse/controller/unit/modules/BaseControllerTest"
    ], function () {
        QUnit.start();
    });
});
