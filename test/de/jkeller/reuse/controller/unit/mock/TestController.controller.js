sap.ui.define([
    "de/jkeller/reuse/controller/BaseController"
],
    function (BaseController) {
        "use strict";

        return BaseController.extend("de.jkeller.reuse.controller.unit.mock.TestController", {
        });
    }
);
