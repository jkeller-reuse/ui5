sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel"
],
    function (Controller, JSONModel) {
        "use strict";

        return Controller.extend("de.jkeller.reuse.control.integration.mock.app.controller.Main", {
            onInit: function() {
                const sampleModel = new JSONModel({duration: ""});
                this.getView().setModel(sampleModel, "sampleModel");
            }
        });
    }
);
