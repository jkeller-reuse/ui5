sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/opaQunit",
    "de/jkeller/reuse/control/integration/arrangements/Startup",
    "de/jkeller/reuse/control/integration/pages/MainPage"
], function(Opa5, opaTest, Startup) {
    "use strict";

    Opa5.extendConfig({
        arrangements: new Startup(),
        pollingInterval: 1
    });

    const SAMPLE_MODEL_NAME = "sampleModel";

    QUnit.module("DurationSlider OPA");

    opaTest("Open DurationSlider value help and should see sliders for time unit", function(Given, When, Then) {
        Given.iStartMyApp();

        When.onTheMainPage.iOpenValueHelpOfInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"});
        Then.onTheMainPage.iShouldSeePopOverWithId("my-duration-slider--popover");
        Then.onTheMainPage.iShouldSeeElementWithLabelFor("Minutes", "sap.m.Slider")
        .and.iShouldSeeElementWithLabelFor("Seconds", "sap.m.Slider")
        .and.iShouldSeeElementWithLabelFor("Millis", "sap.m.Slider");
    });

    opaTest("Enter duration via value help time unit sliders and check DurationSlider value", function(Given, When, Then) {
        When.onTheMainPage.iSlideHandleOfElementWithLabel("Minutes", 1)
        .and.iSlideHandleOfElementWithLabel("Seconds", 36)
        .and.iSlideHandleOfElementWithLabel("Millis", 490);

        Then.onTheMainPage.iElementWithBindingHasValue({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, 
            "01:36.483", "de.jkeller.reuse.control.duration.DurationSlider");
    });

    opaTest("Enter duration via DurationSlider input and check value help slider values", function(Given, When, Then) {
        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, "");
        Then.onTheMainPage.iElementWithBindingHasValue({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, 
            "", "de.jkeller.reuse.control.duration.DurationSlider")
        .and.iElementWithLabelForHasValue("Minutes", 0, "sap.m.Slider")
        .and.iElementWithLabelForHasValue("Seconds", 0, "sap.m.Slider")
        .and.iElementWithLabelForHasValue("Millis", 0, "sap.m.Slider");

        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, "02:15.752");
        
        Then.onTheMainPage.iElementWithBindingHasValue({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, 
            "02:15.752", "de.jkeller.reuse.control.duration.DurationSlider")
        .and.iElementWithLabelForHasValue("Minutes", 2, "sap.m.Slider")
        .and.iElementWithLabelForHasValue("Seconds", 15, "sap.m.Slider")
        .and.iElementWithLabelForHasValue("Millis", 752, "sap.m.Slider");

        When.onTheMainPage.iOpenValueHelpOfInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"});
    });

    opaTest("Enter duration with invalid format and check value state error", function(Given, When, Then){
        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, "2:15.123")
        .and.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, "2:1.123");
        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"});
    });

    opaTest("Clear invalid duration and check value state none", function(Given, When, Then){
        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, "");
        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"});
    });

    opaTest("Enter duration exceeding max and check value state error", function(Given, When, Then){
        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"}, "60:15.752");
        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/duration"});

        Then.iTeardownMyAppFrame();
    });
});
