sap.ui.define([
    "de/jkeller/reuse/control/duration/DurationSlider",
    "de/jkeller/reuse/control/duration/TimeUnit"
], function(DurationSlider, TimeUnit) {
    "use strict";

    const SAMPLE_ID = "sample-id";

    function checkSliderMinMax(assert, slider, unitIdentifier, min, max) {
        assert.equal(slider.getCustomData()[0].getValue().getIdentifier(), unitIdentifier);
        assert.equal(slider.getMin(), min);
        assert.equal(slider.getMax(), max);
    }

    function checkSliderValue(assert, slider, unitIdentifier, value) {
        assert.equal(slider.getCustomData()[0].getValue().getIdentifier(), unitIdentifier);
        assert.equal(slider.getValue(), value);
    }

    function fireInputChange(input) {
        input.fireChange({value: input.getValue()});
    }

    function setSliderValueAndFireChange(slider, value) {
        slider.setValue(value);
        slider.fireChange({value: slider.getValue()});
    }

    QUnit.module("Test DurationSlider", {
        beforeEach: function() {
            this.durationSlider = new DurationSlider();
        },

        afterEach: function() {
            this.durationSlider.destroy();
        }
    });

    QUnit.test("Constructor with properties", function(assert) {
        const durationSlider = new DurationSlider({
            id: SAMPLE_ID,
            blocked: true,
            busy: true,
            busyIndicatorDelay: 500,
            busyIndicatorSize: sap.ui.core.BusyIndicatorSize.Small,
            editable: false,
            enabled: false,
            fieldGroupIds: ["fieldGroup1", "fieldGroup2"],
            name: "myName",
            placeholder: "just some placeholder",
            required: true,
            showValueStateMessage: false,
            textAlign: sap.ui.core.TextAlign.Begin,
            textDirection: sap.ui.core.TextDirection.LTR,
            value: "01:12.951",
            valueState: sap.ui.core.ValueState.Warning,
            valueStateText: "this is a warning",
            visible: false,
            width: "25%",

            timeFormat: "mm:ss.SSS",
            min: "01:10.123",
            max: "89:45.987"
        });

        assert.equal(durationSlider.getId(), SAMPLE_ID);
        assert.ok(durationSlider.getBusy());
        assert.equal(durationSlider.getBusyIndicatorDelay(), 500);
        assert.equal(durationSlider.getBusyIndicatorSize(), sap.ui.core.BusyIndicatorSize.Small);
        assert.notOk(durationSlider.getEditable());
        assert.notOk(durationSlider.getEnabled());
        assert.equal(durationSlider.getName(), "myName");
        assert.equal(durationSlider.getPlaceholder(), "just some placeholder");
        assert.ok(durationSlider.getRequired());
        assert.notOk(durationSlider.getShowValueStateMessage());
        assert.equal(durationSlider.getTextAlign(), sap.ui.core.TextAlign.Begin);
        assert.equal(durationSlider.getTextDirection(), sap.ui.core.TextDirection.LTR);
        assert.equal(durationSlider.getValue(), "01:12.951");
        assert.equal(durationSlider.getValueState(), sap.ui.core.ValueState.Warning);
        assert.equal(durationSlider.getValueStateText(), "this is a warning");
        assert.notOk(durationSlider.getVisible());
        assert.equal(durationSlider.getWidth(), "25%");

        assert.equal(durationSlider.getTimeFormat(), "mm:ss.SSS");
        assert.equal(durationSlider.getMin(), "01:10.123");
        assert.equal(durationSlider._getMinHours(), 0);
        assert.equal(durationSlider._getMinMinutes(), 1);
        assert.equal(durationSlider._getMinSeconds(), 0);
        assert.equal(durationSlider._getMinMillis(), 0);
        assert.equal(durationSlider.getMax(), "89:45.987");
        assert.equal(durationSlider._getMaxHours(), undefined);
        assert.equal(durationSlider._getMaxMinutes(), 89);
        assert.equal(durationSlider._getMaxSeconds(), 59);
        assert.equal(durationSlider._getMaxMillis(), 999);

        durationSlider.destroy();
    });

    QUnit.test("Constructor with duration slider specific default properties", function(assert) {
        const durationSlider = new DurationSlider();

        assert.equal(durationSlider.getTimeFormat(), "HH:mm:ss.SSS");
        assert.equal(durationSlider.getMin(), "00:00:00.000");
        assert.equal(durationSlider._getMinHours(), 0);
        assert.equal(durationSlider._getMinMinutes(), 0);
        assert.equal(durationSlider._getMinSeconds(), 0);
        assert.equal(durationSlider._getMinMillis(), 0);
        assert.equal(durationSlider.getMax(), "23:59:59.999");
        assert.equal(durationSlider._getMaxHours(), 23);
        assert.equal(durationSlider._getMaxMinutes(), 59);
        assert.equal(durationSlider._getMaxSeconds(), 59);
        assert.equal(durationSlider._getMaxMillis(), 999);
    });

    QUnit.test("Constructor with id and default properties", function(assert) {
        const durationSlider = new DurationSlider(SAMPLE_ID);

        assert.equal(durationSlider.getTimeFormat(), "HH:mm:ss.SSS");
        assert.equal(durationSlider.getMin(), "00:00:00.000");
        assert.equal(durationSlider._getMinHours(), 0);
        assert.equal(durationSlider._getMinMinutes(), 0);
        assert.equal(durationSlider._getMinSeconds(), 0);
        assert.equal(durationSlider._getMinMillis(), 0);
        assert.equal(durationSlider.getMax(), "23:59:59.999");
        assert.equal(durationSlider._getMaxHours(), 23);
        assert.equal(durationSlider._getMaxMinutes(), 59);
        assert.equal(durationSlider._getMaxSeconds(), 59);
        assert.equal(durationSlider._getMaxMillis(), 999);

        durationSlider.destroy();
    });

    QUnit.test("Constructor with id and duration slider properties", function(assert) {
        const durationSlider = new DurationSlider(SAMPLE_ID, {
            timeFormat: "mm:ss.SSS",
            min: "01:10.123",
            max: "89:45.987"
        });

        assert.equal(durationSlider.getId(), SAMPLE_ID);

        assert.equal(durationSlider.getTimeFormat(), "mm:ss.SSS");
        assert.equal(durationSlider.getMin(), "01:10.123");
        assert.equal(durationSlider._getMinHours(), 0);
        assert.equal(durationSlider._getMinMinutes(), 1);
        assert.equal(durationSlider._getMinSeconds(), 0);
        assert.equal(durationSlider._getMinMillis(), 0);
        assert.equal(durationSlider.getMax(), "89:45.987");
        assert.equal(durationSlider._getMaxHours(), undefined);
        assert.equal(durationSlider._getMaxMinutes(), 89);
        assert.equal(durationSlider._getMaxSeconds(), 59);
        assert.equal(durationSlider._getMaxMillis(), 999);

        durationSlider.destroy();
    });

    QUnit.test("Set invalid time format expect time format not set", function(assert) {
        const durationSlider = new DurationSlider(SAMPLE_ID, {
            timeFormat: "MM:ss.SSS"
        });

        assert.equal(durationSlider.getTimeFormat(), "HH:mm:ss.SSS");  
        
        durationSlider.setTimeFormat("HH:MM:sss");

        assert.equal(durationSlider.getTimeFormat(), "HH:mm:ss.SSS");
        durationSlider.destroy();
    });

    QUnit.test("Set invalid time format with '-' expect time format not set", function(assert) {
        const durationSlider = new DurationSlider(SAMPLE_ID, {
            timeFormat: "mm-ss.SSS"
        });

        assert.equal(durationSlider.getTimeFormat(), "HH:mm:ss.SSS");  
        
        durationSlider.setTimeFormat("HH:mm-sss");

        assert.equal(durationSlider.getTimeFormat(), "HH:mm:ss.SSS");
        durationSlider.destroy();
    });

    QUnit.test("set different time format and check sliders setup", function(assert) {
        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Hours, 0, 23);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 0, 59);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 59);
        slider = sliders[3];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("H:m:s.SS");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Hours, 0, 9);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 0, 9);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 9);
        slider = sliders[3];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 99);

        this.durationSlider.setTimeFormat("mmm:ss.S");
        this.durationSlider.setMax("999:59.9");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 3);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 0, 999);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 59);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 9);

        this.durationSlider.setTimeFormat("ssss.SS");
        this.durationSlider.setMax("9999.99");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 2);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 9999);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 99);
    });

    QUnit.test("set valid min value check sliders", function(assert) {
        this.durationSlider.setMin("01:36:12.358");
        assert.equal(this.durationSlider.getMin(), "01:36:12.358");
        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Hours, 1, 23);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 0, 59);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 59);
        slider = sliders[3];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("mm:ss.SSS");
        this.durationSlider.setMin("36:12.358");
        assert.equal(this.durationSlider.getMin(), "36:12.358");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 3);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 36, 59);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 59);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("ss.SSS");
        this.durationSlider.setMin("12.358");
        assert.equal(this.durationSlider.getMin(), "12.358");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 2);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 12, 59);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("SSS");
        this.durationSlider.setMin("358");
        assert.equal(this.durationSlider.getMin(), "358");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 1);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 358, 999);
    });

    QUnit.test("set valid max value check sliders", function(assert) {
        this.durationSlider.setMax("01:36:12.358");
        assert.equal(this.durationSlider.getMax(), "01:36:12.358");
        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Hours, 0, 1);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 0, 59);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 59);
        slider = sliders[3];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("mm:ss.SSS");
        this.durationSlider.setMax("36:12.358");
        assert.equal(this.durationSlider.getMax(), "36:12.358");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 3);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Minutes, 0, 36);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 59);
        slider = sliders[2];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("ss.SSS");
        this.durationSlider.setMax("12.358");
        assert.equal(this.durationSlider.getMax(), "12.358");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 2);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Seconds, 0, 12);
        slider = sliders[1];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 999);

        this.durationSlider.setTimeFormat("SSS");
        this.durationSlider.setMax("358");
        assert.equal(this.durationSlider.getMax(), "358");
        sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 1);
        slider = sliders[0];
        checkSliderMinMax(assert, slider, TimeUnit.Identifier.Millis, 0, 358);
    });

    QUnit.test("set value and check sliders", function(assert) {
        let expectedValue = "00:00:00.125";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 0);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 0);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 0);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);

        expectedValue = "00:00:38.125";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 0);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 0);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 38);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);

        expectedValue = "00:12:38.125";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 0);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 12);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 38);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);

        expectedValue = "04:12:38.125";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 4);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 12);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 38);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);
    });

    QUnit.test("set invalid value, value too small, check value state", function(assert) {
        let expectedValue = "01:36:12.357";
        this.durationSlider.setMin("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("is less than"));
        assert.ok(this.durationSlider.getAggregation("_input").getValueStateText().includes("is less than"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("is less than"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.None);
        assert.equal(this.durationSlider.getValueStateText(), "");
        assert.equal(this.durationSlider.getAggregation("_input").getValueStateText(), "");
        assert.equal(this.durationSlider._getPopoverValueStateTextElement().getText(), "");
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:12.359";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.None);
        assert.equal(this.durationSlider.getValueStateText(), "");
        assert.equal(this.durationSlider.getAggregation("_input").getValueStateText(), "");
        assert.equal(this.durationSlider._getPopoverValueStateTextElement().getText(), "");
        assert.equal(this.durationSlider.getValue(), expectedValue);
    });

    QUnit.test("set invalid value, value too small, check validation error fired", function(assert) {
        let finished = assert.async();
        let isValidationErrorCalled = false;
        this.durationSlider.attachValidationError((event) => {
            isValidationErrorCalled = true;
            finished();
        });
        
        let expectedValue = "01:36:12.357";
        this.durationSlider.setMin("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationErrorCalled);
    });

    QUnit.test("set invalid value, value too big, check value state", function(assert) {
        let expectedValue = "01:36:12.359";
        let input = this.durationSlider.getAggregation("_input");
        this.durationSlider.setMax("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("is greater than"));
        assert.ok(input.getValueStateText().includes("is greater than"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("is greater than"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.None);
        assert.equal(this.durationSlider.getValueStateText(), "");
        assert.equal(input.getValueStateText(), "");
        assert.equal(this.durationSlider._getPopoverValueStateTextElement().getText(), "");
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:12.357";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.None);
        assert.equal(this.durationSlider.getValueStateText(), "");
        assert.equal(input.getValueStateText(), "");
        assert.equal(this.durationSlider._getPopoverValueStateTextElement().getText(), "");
        assert.equal(this.durationSlider.getValue(), expectedValue);
    });

    QUnit.test("set invalid value, value too big, check validation error fired", function(assert) {
        let finished = assert.async();
        let isValidationErrorCalled = false;
        this.durationSlider.attachValidationError((event) => {
            isValidationErrorCalled = true;
            finished();
        });
        
        let expectedValue = "01:36:12.359";
        this.durationSlider.setMax("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationErrorCalled);
    });

    QUnit.test("set invalid value and fix it, check validation success fired", function(assert) {
        let finishedValidationError = assert.async();
        let isValidationErrorCalled = false;
        this.durationSlider.attachValidationError((event) => {
            isValidationErrorCalled = true;
            finishedValidationError();
        });
        let finishedValidationSuccess = assert.async();
        let isValidationSuccessCalled = false;
        this.durationSlider.attachValidationError((event) => {
            isValidationSuccessCalled = true;
            finishedValidationSuccess();
        });

        let expectedValue = "01:36:12.359";
        this.durationSlider.setMax("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationErrorCalled);

        expectedValue = "01:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationSuccessCalled);

        finishedValidationError = assert.async();
        isValidationErrorCalled = false;
        finishedValidationSuccess = assert.async();
        isValidationSuccessCalled = false;
        expectedValue = "01:36:12.357";
        this.durationSlider.setMin("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationErrorCalled);

        expectedValue = "01:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationSuccessCalled);
    });

    QUnit.test("set invalid value and clear it, check validation success fired", function(assert) {
        let finishedValidationError = assert.async();
        let isValidationErrorCalled = false;
        this.durationSlider.attachValidationError((event) => {
            isValidationErrorCalled = true;
            finishedValidationError();
        });
        let finishedValidationSuccess = assert.async();
        let isValidationSuccessCalled = false;
        this.durationSlider.attachValidationError((event) => {
            isValidationSuccessCalled = true;
            finishedValidationSuccess();
        });

        let expectedValue = "01:36:12.359";
        this.durationSlider.setMax("01:36:12.358");
        this.durationSlider.setValue(expectedValue);
        assert.ok(isValidationErrorCalled);

        this.durationSlider.setValue("");
        assert.ok(isValidationSuccessCalled);
    });

    QUnit.test("set invalid value, value not expected format, check value state", function(assert) {
        let expectedValue = "asdf";
        let input = this.durationSlider.getAggregation("_input");
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("Entered duration 'asdf' does not match format"));
        assert.ok(input.getValueStateText().includes("Entered duration 'asdf' does not match format"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("Entered duration 'asdf' does not match format"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "123:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("Entered duration '123:36:12.358' does not match format"));
        assert.ok(input.getValueStateText().includes("Entered duration '123:36:12.358' does not match format"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("Entered duration '123:36:12.358' does not match format"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:360:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("Entered duration '01:360:12.358' does not match format"));
        assert.ok(input.getValueStateText().includes("Entered duration '01:360:12.358' does not match format"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("Entered duration '01:360:12.358' does not match format"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:125.358";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("Entered duration '01:36:125.358' does not match format"));
        assert.ok(input.getValueStateText().includes("Entered duration '01:36:125.358' does not match format"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("Entered duration '01:36:125.358' does not match format"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:125.3589";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.Error);
        assert.ok(this.durationSlider.getValueStateText().includes("Entered duration '01:36:125.3589' does not match format"));
        assert.ok(input.getValueStateText().includes("Entered duration '01:36:125.3589' does not match format"));
        assert.ok(this.durationSlider._getPopoverValueStateTextElement().getText().includes("Entered duration '01:36:125.3589' does not match format"));
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "01:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.equal(this.durationSlider.getValueState(), sap.ui.core.ValueState.None);
        assert.equal(this.durationSlider.getValueStateText(), "");
        assert.equal(input.getValueStateText(), "");
        assert.equal(this.durationSlider._getPopoverValueStateTextElement().getText(), "");
        assert.equal(this.durationSlider.getValue(), expectedValue);
    });

    QUnit.test("set invalid value, value not expected format, check format error fired", function(assert) {
        let finished = assert.async();
        let isFormatErrorCalled = false;
        this.durationSlider.attachFormatError((event) => {
            isFormatErrorCalled = true;
            finished();
        });
        
        let expectedValue = "asdf";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isFormatErrorCalled);

        isFormatErrorCalled = false;
        finished = assert.async();
        expectedValue = "123:36:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isFormatErrorCalled);

        isFormatErrorCalled = false;
        finished = assert.async();
        expectedValue = "01:360:12.358";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isFormatErrorCalled);

        isFormatErrorCalled = false;
        finished = assert.async();
        expectedValue = "01:36:125.358";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isFormatErrorCalled);

        isFormatErrorCalled = false;
        finished = assert.async();
        expectedValue = "01:36:125.3589";
        this.durationSlider.setValue(expectedValue);
        assert.ok(isFormatErrorCalled);
    });

    QUnit.test("set value via input and check sliders", function(assert) {
        let expectedValue = "00:00:00.125";
        let input = this.durationSlider.getAggregation("_input");
        input.setValue(expectedValue);
        fireInputChange(input);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 0);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 0);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 0);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);

        expectedValue = "00:00:38.125";
        input.setValue(expectedValue);
        fireInputChange(input);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 0);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 0);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 38);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);

        expectedValue = "00:12:38.125";
        input.setValue(expectedValue);
        fireInputChange(input);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 0);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 12);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 38);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);

        expectedValue = "04:12:38.125";
        input.setValue(expectedValue);
        fireInputChange(input);
        assert.equal(this.durationSlider.getValue(), expectedValue);
        slider = sliders[0];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Hours, 4);
        slider = sliders[1];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Minutes, 12);
        slider = sliders[2];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Seconds, 38);
        slider = sliders[3];
        checkSliderValue(assert, slider, TimeUnit.Identifier.Millis, 125);
    });

    QUnit.test("set value via slider and check input", function(assert) {
        let expectedValue = "00:00:00.125";
        let input = this.durationSlider.getAggregation("_input");
        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[3];
        setSliderValueAndFireChange(slider, 125);
        assert.equal(input.getValue(), expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "00:00:38.125";
        slider = sliders[2];
        setSliderValueAndFireChange(slider, 38);
        assert.equal(input.getValue(), expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "00:12:38.125";
        slider = sliders[1];
        setSliderValueAndFireChange(slider, 12);
        assert.equal(input.getValue(), expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);

        expectedValue = "04:12:38.125";
        slider = sliders[0];
        setSliderValueAndFireChange(slider, 4);
        assert.equal(input.getValue(), expectedValue);
        assert.equal(this.durationSlider.getValue(), expectedValue);
    });

    QUnit.test("get duration in millis", function(assert) {
        assert.equal(Number(this.durationSlider.getDurationMillis()), 0);

        this.durationSlider.setValue("04:12:38.125");
        assert.equal(Number(this.durationSlider.getDurationMillis()), 15158125);
    });

    QUnit.test("attach change, change via input triggered", function(assert) {
        let expectedValue = "04:12:38.125";

        let finished1 = assert.async();
        let isChange1Called = false;
        this.durationSlider.attachChange((event) => {
            assert.equal(this.durationSlider.getValue(), expectedValue);
            isChange1Called = true;
            finished1();
        });
        let finished2 = assert.async();
        let isChange2Called = false;
        this.durationSlider.attachChange((event) => {
            assert.equal(this.durationSlider.getValue(), expectedValue);
            isChange2Called = true;
            finished2();
        });
        
        let input = this.durationSlider.getAggregation("_input");
        input.setValue(expectedValue);
        fireInputChange(input);

        assert.ok(isChange1Called);
        assert.ok(isChange2Called);
    });

    QUnit.test("attach change, change via slider triggered", function(assert) {
        let expectedValue = "00:00:00.125";

        let finished1 = assert.async();
        let isChange1Called = false;
        this.durationSlider.attachChange((event) => {
            assert.equal(this.durationSlider.getValue(), expectedValue);
            isChange1Called = true;
            finished1();
        });
        let finished2 = assert.async();
        let isChange2Called = false;
        this.durationSlider.attachChange((event) => {
            assert.equal(this.durationSlider.getValue(), expectedValue);
            isChange2Called = true;
            finished2();
        });

        let sliders = this.durationSlider.getTimeUnitSliders();
        assert.equal(sliders.length, 4);
        let slider = sliders[3];
        setSliderValueAndFireChange(slider, 125);
        assert.ok(isChange1Called);
        assert.ok(isChange2Called);

        expectedValue = "00:00:38.125";
        finished1 = assert.async();
        isChange1Called = false;
        finished2 = assert.async();
        isChange2Called = false;
        slider = sliders[2];
        setSliderValueAndFireChange(slider, 38);
        assert.ok(isChange1Called);
        assert.ok(isChange2Called);

        expectedValue = "00:12:38.125";
        finished1 = assert.async();
        isChange1Called = false;
        finished2 = assert.async();
        isChange2Called = false;
        slider = sliders[1];
        setSliderValueAndFireChange(slider, 12);
        assert.ok(isChange1Called);
        assert.ok(isChange2Called);

        expectedValue = "04:12:38.125";
        finished1 = assert.async();
        isChange1Called = false;
        finished2 = assert.async();
        isChange2Called = false;
        slider = sliders[0];
        setSliderValueAndFireChange(slider, 4);
        assert.ok(isChange1Called);
        assert.ok(isChange2Called);
    });

    QUnit.test("constructor non default time format no max provided, check max default", function(assert) {
        let durationSlider = new DurationSlider({timeFormat: "mm:ss.SSS"});
        assert.equal(durationSlider.getMax(), "59:59.999");
        assert.equal(Number(durationSlider._getMaxInMillis()), 3599999);

        durationSlider = new DurationSlider({timeFormat: "m:ss.SS"});
        assert.equal(durationSlider.getMax(), "9:59.99");
        assert.equal(Number(durationSlider._getMaxInMillis()), 599099);
    });

    QUnit.test("constructor non default time format no min provided, check min default", function(assert) {
        let durationSlider = new DurationSlider({timeFormat: "mm:ss.SSS"});
        assert.equal(durationSlider.getMin(), "00:00.000");
        assert.equal(Number(durationSlider._getMinInMillis()), 0);

        durationSlider = new DurationSlider({timeFormat: "m:ss.SS"});
        assert.equal(durationSlider.getMin(), "0:00.00");
        assert.equal(Number(durationSlider._getMinInMillis()), 0);
    });
});