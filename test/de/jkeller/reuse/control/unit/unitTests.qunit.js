QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "de/jkeller/reuse/control/unit/modules/DurationSliderTest",
    ], function () {
        QUnit.start();
    });
});
