sap.ui.define([
    "sap/ui/test/Opa5",
    "sap/ui/test/opaQunit",
    "de/jkeller/reuse/validator/integration/arrangements/Startup",
    "de/jkeller/reuse/validator/integration/pages/MainPage"
], function(Opa5, opaTest, Startup) {
    "use strict";

    Opa5.extendConfig({
        arrangements: new Startup(),
        pollingInterval: 1
    });

    const SAMPLE_MODEL_NAME = "sampleModel";

    QUnit.module("Validator - simple");

    opaTest("Trigger validation with errors, should see validation errors", function(Given, When, Then) {
        Given.iStartMyApp();

        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"}, "0");

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"});

        When.onTheMainPage.iClickButtonWithLabel("Validate");

        Then.onTheMainPage.iShouldSeeDialogWithTitle("Error!", "Inputs are not valid!");
        When.onTheMainPage.iClickDialogButtonWithLabel("Custom Close");

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"})
        .and.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"})
        .and.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/optional"});
    });

    opaTest("Correct errors, then should see no validation errors", function(Given, When, Then) {
        let currentDate = new Date();
        let dateString = `${currentDate.getDate()}.${currentDate.getMonth() + 1}.${currentDate.getFullYear()}`;

        When.onTheMainPage.iSelectItemFromSelectWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"}, "2")
        .and.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"}, "18")
        .and.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"}, dateString);

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/optional"});
    });

    opaTest("re-validate after errors corrected, should not see validation errors", function(Given, When, Then) {
        When.onTheMainPage.iClickButtonWithLabel("Validate");

        Then.onTheMainPage.iShouldSeeDialogWithTitle("Info", "No Validation Errors!");
        When.onTheMainPage.iClickDialogButtonWithLabel("Custom Close");

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/optional"});

        Then.iTeardownMyAppFrame();
    });

    QUnit.module("Validator - receiving invalid inputs");

    opaTest("Trigger validation (receiving invalid inputs) with errors, should see validation errors", function(Given, When, Then) {
        Given.iStartMyApp();

        When.onTheMainPage.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"}, "0");

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"});

        When.onTheMainPage.iClickButtonWithLabel("Validate Receiving Input");

        Then.onTheMainPage.iShouldSeeDialogWithTitle("Error!", "3 inputs are not valid!");
        When.onTheMainPage.iClickDialogButtonWithLabel("Custom Close");

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"})
        .and.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"})
        .and.iShouldSeeElementWithBindingAndValueStateError({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/optional"});
    });

    opaTest("Correct errors, then should see no validation errors (receiving invalid inputs)", function(Given, When, Then) {
        let currentDate = new Date();
        let dateString = `${currentDate.getDate()}.${currentDate.getMonth() + 1}.${currentDate.getFullYear()}`;

        When.onTheMainPage.iSelectItemFromSelectWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"}, "2")
        .and.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"}, "18")
        .and.iEnterTextInInputWithBinding({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"}, dateString);

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/optional"});
    });

    opaTest("re-validate after errors corrected, should not see validation errors (receiving invalid inputs)", function(Given, When, Then) {
        When.onTheMainPage.iClickButtonWithLabel("Validate Receiving Input");

        Then.onTheMainPage.iShouldSeeDialogWithTitle("Info", "No Validation Errors!");
        When.onTheMainPage.iClickDialogButtonWithLabel("Custom Close");

        Then.onTheMainPage.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/select/id"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/number"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/date"})
        .and.iShouldSeeElementWithBindingAndValueStateNone({modelName: SAMPLE_MODEL_NAME, propertyPath: "/optional"});

        Then.iTeardownMyAppFrame();
    });
});
