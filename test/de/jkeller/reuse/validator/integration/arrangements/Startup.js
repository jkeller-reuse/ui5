sap.ui.define([
    "sap/ui/test/Opa5"
], function(Opa5) {
    "use strict";

    return Opa5.extend("de.jkeller.reuse.validator.integration.arrangements.Startup", {
        iStartMyApp: function (options) {
            let settings = {
                source: "./mock/app/index.html"
            }

            if(options) {
                if(options.width) {
                    settings.width = options.width;
                }

                if(options.height) {
                    settings.height = options.height;
                }
            }

            this.iStartMyAppInAFrame(settings);
        }
    });
});
