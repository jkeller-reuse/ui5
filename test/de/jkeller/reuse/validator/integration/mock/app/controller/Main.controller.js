sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "de/jkeller/reuse/validator/SimpleValidator",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/resource/ResourceModel",
    "sap/m/MessageBox"
],
    function (Controller, SimpleValidator, JSONModel, ResourceModel, MessageBox) {
        "use strict";

        return Controller.extend("de.jkeller.reuse.validator.integration.mock.app.controller.Main", {
            onInit: function() {
                this.simpleValidator = new SimpleValidator();

                this.getView().setModel(new ResourceModel({bundleName: "de.jkeller.reuse.validator.integration.mock.app.i18n.i18n"}), "i18n");

                let sampleModel = new JSONModel({
                    select: {
                    },
                    number: 5
                });
                this.getView().setModel(sampleModel, "sampleModel");

                let sampleSelectData = new JSONModel([
                    {id: 1, name: "First"},
                    {id: 2, name: "Second"},
                    {id: 3, name: "Entry"}
                ]);
                this.getView().setModel(sampleSelectData, "sampleSelectData");
            },

            onValidate: function() {
                let sampleForm = this.getView().byId("sample-form");
                let valid = this.simpleValidator.validate(sampleForm);
                if(valid) {
                    MessageBox.confirm(this.getView().getModel("i18n").getProperty("noValidationError"), {
                        title: this.getView().getModel("i18n").getProperty("noValidationErrorDialogTitle"),
                        icon: MessageBox.Icon.INFORMATION,
                        initialFocus: "Custom Close",
                        actions: ["Custom Close"],
                        onClose: (sAction) => {
                        }
                    });
                } else {
                    MessageBox.confirm(this.getView().getModel("i18n").getProperty("validationError"), {
                        title: this.getView().getModel("i18n").getProperty("errorDialogTitle"),
                        icon: MessageBox.Icon.ERROR,
                        initialFocus: "Custom Close",
                        actions: ["Custom Close"],
                        onClose: (sAction) => {
                        }
                    });
                }
            },

            onValidateReceivingInputs: function() {
                const sampleForm = this.getView().byId("sample-form");
                const invalidInputs = this.simpleValidator.validateReceivingInvalid(sampleForm);
                if(invalidInputs && invalidInputs.length === 0) {
                    MessageBox.confirm(this.getView().getModel("i18n").getProperty("noValidationError"), {
                        title: this.getView().getModel("i18n").getProperty("noValidationErrorDialogTitle"),
                        icon: MessageBox.Icon.INFORMATION,
                        initialFocus: "Custom Close",
                        actions: ["Custom Close"],
                        onClose: (sAction) => {
                        }
                    });
                } else {
                    MessageBox.confirm(this.getView().getModel("i18n").getResourceBundle().getText("validationErrorNumErrors", [invalidInputs.length]), {
                        title: this.getView().getModel("i18n").getProperty("errorDialogTitle"),
                        icon: MessageBox.Icon.ERROR,
                        initialFocus: "Custom Close",
                        actions: ["Custom Close"],
                        onClose: (sAction) => {
                        }
                    });
                }
            }
        });
    }
);
