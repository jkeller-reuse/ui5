/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "de/jkeller/reuse/validator/integration/journeys/MainJourney"
    ], function() {
        QUnit.start();
    });
});
