QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
    "use strict";

    sap.ui.require([
        "de/jkeller/reuse/model/unit/modules/JSONModelExtendedTest"
    ], function () {
        QUnit.start();
    });
});
