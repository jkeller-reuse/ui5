sap.ui.define([
    "de/jkeller/reuse/model/JSONModelExtended",
], function(JSONModelExtended) {
    "use strict";

    const TEST_URL = "/myTest";
    const TEST_URL_REGEX = /myTest(\/|\/\?.*|\?\.*)?/;
    const TEST_URL_RESPONSE_JSON = {  my: "very", awesome: "JSON" };
    const ERROR_URL = "/failing";
    const ERROR_URL_REGEX = /failing(\/|\/\?.*|\?\.*)?/;

    const BASE_URL = `${window.location.origin}`;
    const ABSOLUTE_TEST_URL = `${BASE_URL}${TEST_URL}`;

    const TEST_DATA = {
        COMMON: {some: "test", content: "here"},
        SEND: {sent: "this", additional: "information"},
        PROCESSED: {was: "processed"},
        MERGE: {merge: "data"},
        URL_PARAMETERS: { param: "parameter1", awesome: true }
    }

    QUnit.module("Test JSONModelExtended", {
        before: function() {
            this.fakeServer = sinon.fakeServer.create();
            this.fakeServer.xhr.useFilters = true;

            this.fakeServer.xhr.addFilter(function(method, url) {
                return !url.match(/myTest(\/|\/\?.*|\?\.*)?|failing(\/|\/\?.*|\?\.*)?/);
            });

            this.fakeServer.respondWith("GET", TEST_URL_REGEX, [
                200, 
                { "Content-Type": "application/json" },
                JSON.stringify(TEST_URL_RESPONSE_JSON)
            ]);

            this.fakeServer.respondWith("POST", TEST_URL_REGEX, function(xhr) {
                let responseData = {
                    ...TEST_DATA.PROCESSED, 
                    ... JSON.parse(xhr.requestBody)
                };
                let parameters = (new URL(xhr.url)).searchParams;
                parameters = parameters? parameters.toString(): "";
                if(parameters) {
                    responseData = {
                        ... responseData,
                        parameters: parameters
                    }
                }

                xhr.respond(
                    200,
                    {"Content-Type": "application/json"},
                    JSON.stringify(responseData)
                );
            });

            this.fakeServer.respondWith("GET", ERROR_URL_REGEX, [
                400,
                { "Content-Type": "application/json" },
                JSON.stringify({error: "super bad error happened"})
            ]);

            this.fakeServer.respondWith("POST", ERROR_URL_REGEX, [
                400,
                { "Content-Type": "application/json" },
                JSON.stringify({error: "super bad error happened"})
            ]);
        },

        after: function() {
            this.fakeServer.restore();
        }
    });

    QUnit.test("Constructor with data", function(assert) {
        let expData = {
            id: 18,
            some: "test",
            more: "test stuff"
        }

        let jsonModel = new JSONModelExtended(expData);

        assert.equal(jsonModel.getJSON(), JSON.stringify(expData));
    });

    QUnit.test("Constructor string base Url true", function(assert) {
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);

        assert.equal(jsonModel._baseURL, ABSOLUTE_TEST_URL);
        assert.equal(jsonModel.getJSON(), JSON.stringify({}));
    });

    QUnit.test("Constructor string base Url false", function(assert) {
        let jsonModel = new JSONModelExtended(TEST_URL, false);

        assert.equal(jsonModel._baseURL, "");
        this.fakeServer.respond();
    });

    QUnit.test("load data synch via base URL, merge false", function(assert) {        
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.load("", {async: false});

        assert.equal(jsonModel.getJSON(), JSON.stringify(TEST_URL_RESPONSE_JSON));
    });

    QUnit.test("load data synch, merge false", function(assert) {        
        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.load(TEST_URL, {async: false});

        assert.equal(jsonModel.getJSON(), JSON.stringify(TEST_URL_RESPONSE_JSON));
    });

    QUnit.test("load data synch, merge true", function(assert) {
        let expData = {
            ... TEST_DATA.COMMON,
            ... TEST_URL_RESPONSE_JSON
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        jsonModel.load(TEST_URL, {async: false, merge: true});
        

        assert.equal(jsonModel.getJSON(), JSON.stringify(expData));
    });

    QUnit.test("call GET synch, merge false", function(assert) {
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        let answer = jsonModel.call("", "GET", {async: false});
        

        assert.equal(JSON.stringify(answer), JSON.stringify(TEST_URL_RESPONSE_JSON));
    });

    QUnit.test("call GET synch, merge true", function(assert) {
        let expMergedData = {
            ... TEST_DATA.COMMON,
            ... TEST_URL_RESPONSE_JSON
        }

        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        let answer = jsonModel.call("", "GET", {async: false, merge: true});
        

        assert.equal(JSON.stringify(answer), JSON.stringify(TEST_URL_RESPONSE_JSON));
        assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
    });

    QUnit.test("call POST with JSON object data synch, merge false", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        let answer = jsonModel.call(TEST_URL, "POST", {async: false}, TEST_DATA.COMMON);

        assert.equal(JSON.stringify(answer), JSON.stringify(expData));
    });

    QUnit.test("call POST with JSON string data synch, merge false", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        let answer = jsonModel.call(TEST_URL, "POST", {async: false}, JSON.stringify(TEST_DATA.COMMON));

        assert.equal(JSON.stringify(answer), JSON.stringify(expData));
    });

    QUnit.test("call POST with JSON object data synch, merge true", function(assert) {
        let expAnswer = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }
        let expMergedData = {
            ... TEST_DATA.MERGE,
            ... expAnswer
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.MERGE);
        let answer = jsonModel.call(TEST_URL, "POST", {async: false, merge: true}, TEST_DATA.COMMON);

        assert.equal(JSON.stringify(answer), JSON.stringify(expAnswer));
        assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
    });

    QUnit.test("call POST with JSON string data synch, merge true", function(assert) {
        let expAnswer = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }
        let expMergedData = {
            ... TEST_DATA.MERGE,
            ... expAnswer
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.MERGE);
        let answer = jsonModel.call(TEST_URL, "POST", {async: false, merge: true}, JSON.stringify(TEST_DATA.COMMON));

        assert.equal(JSON.stringify(answer), JSON.stringify(expAnswer));
        assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
    });

    QUnit.test("call GET async, merge false", function(assert) {        
        let finished = assert.async();
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.call("", "GET", {
            async: true, 
            success: function(responseData) {
                assert.equal(JSON.stringify(responseData), JSON.stringify(TEST_URL_RESPONSE_JSON));
                finished();
            },
            error: function() {
                assert.ok(false);
                finished();
            }
        });

        this.fakeServer.respond();
    });

    QUnit.test("call GET async, merge true", function(assert) {
        let expMergedData = {
            ... TEST_DATA.COMMON,
            ... TEST_URL_RESPONSE_JSON
        }

        let finished = assert.async();
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        jsonModel.call("", "GET", {
            async: true, 
            merge: true,
            success: function(responseData) {
                assert.equal(JSON.stringify(responseData), JSON.stringify(TEST_URL_RESPONSE_JSON));
                assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
                finished();
            },
            error: function() {
                assert.ok(false);
                finished();
            }
        });

        this.fakeServer.respond();
    });

    QUnit.test("call GET async error", function(assert) {        
        let finished = assert.async();
        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.call(ERROR_URL, "GET", {
            async: true, 
            success: function() {
                assert.ok(false);
                finished();
            },
            error: function() {
                assert.ok(true);
                finished();
            },
        });

        this.fakeServer.respond();
    });

    QUnit.test("create synch, merge false, no supplied data", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        let answer = jsonModel.create(TEST_URL, {async: false});

        assert.equal(jsonModel.getJSON(), JSON.stringify(TEST_DATA.COMMON));
        assert.equal(JSON.stringify(answer), JSON.stringify(expData));
    });

    QUnit.test("create synch, merge false, with supplied data", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.SEND
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        let answer = jsonModel.create(TEST_URL, {async: false}, TEST_DATA.SEND);

        assert.equal(jsonModel.getJSON(), JSON.stringify(TEST_DATA.COMMON));
        assert.equal(JSON.stringify(answer), JSON.stringify(expData));
    });

    QUnit.test("create synch, merge true, no supplied data", function(assert) {
        let expData = {
            ... TEST_DATA.COMMON,
            ... TEST_DATA.PROCESSED
        }
        let expAnswer = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        let answer = jsonModel.create(TEST_URL, {async: false, merge: true});

        assert.equal(jsonModel.getJSON(), JSON.stringify(expData));
        assert.equal(JSON.stringify(answer), JSON.stringify(expAnswer));
    });

    QUnit.test("create synch, merge true, with supplied data", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.SEND
        }
        let expMergedData = {
            ... TEST_DATA.COMMON,
            ... expData
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        let answer = jsonModel.create(TEST_URL, {async: false, merge: true}, TEST_DATA.SEND);

        assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
        assert.equal(JSON.stringify(answer), JSON.stringify(expData));
    });

    QUnit.test("create async, merge false", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }
        
        let finished = assert.async();
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        jsonModel.create("", {
            async: true, 
            success: function(responseData) {
                assert.equal(JSON.stringify(responseData), JSON.stringify(expData));
                finished();
            },
            error: function() {
                assert.ok(false);
                finished();
            }
        });

        this.fakeServer.respond();
    });

    QUnit.test("create async, merge true", function(assert) {
        let expData = {
            ... TEST_DATA.COMMON,
            ... TEST_DATA.PROCESSED
        }
        let expAnswer = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }

        let finished = assert.async();
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        jsonModel.create("", {
            async: true, 
            merge: true,
            success: function(responseData) {
                assert.equal(JSON.stringify(responseData), JSON.stringify(expAnswer));
                assert.equal(jsonModel.getJSON(), JSON.stringify(expData));
                finished();
            },
            error: function() {
                assert.ok(false);
                finished();
            }
        });

        this.fakeServer.respond();
    });

    QUnit.test("create async error", function(assert) {        
        let finished = assert.async();
        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.create(ERROR_URL, {
            async: true, 
            success: function() {
                assert.ok(false);
                finished();
            },
            error: function() {
                assert.ok(true);
                finished();
            },
        });

        this.fakeServer.respond();
    });

    QUnit.test("create synch, merge false, with supplied JSON string", function(assert) {
        let expData = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.SEND
        }

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData(TEST_DATA.COMMON);
        let answer = jsonModel.create(TEST_URL, {async: false}, JSON.stringify(TEST_DATA.SEND));

        assert.equal(jsonModel.getJSON(), JSON.stringify(TEST_DATA.COMMON));
        assert.equal(JSON.stringify(answer), JSON.stringify(expData));
    });

    QUnit.test("call with URL parameters", function(assert) {
        let expAnswer = { 
            ... TEST_DATA.PROCESSED,
            parameters: "param=parameter1&awesome=true"
        }

        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        let answer = jsonModel.call("", "POST", {
            async: false, 
            urlParameters: TEST_DATA.URL_PARAMETERS,
        });

        assert.equal(JSON.stringify(answer), JSON.stringify(expAnswer));
    });

    QUnit.test("create with URL parameters", function(assert) {
        let expAnswer = { 
            ... TEST_DATA.PROCESSED,
            parameters: "param=parameter1&awesome=true"
        }

        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        let answer = jsonModel.create("", {
            async: false, 
            urlParameters: TEST_DATA.URL_PARAMETERS,
        });

        assert.equal(JSON.stringify(answer), JSON.stringify(expAnswer));
    });

    QUnit.test("create synch, merge array, with supplied data", function(assert) {
        let expAnswer = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.SEND
        }
        let expMergedData = [
            expAnswer, 
            TEST_DATA.COMMON
        ];

        let jsonModel = new JSONModelExtended(BASE_URL, true);
        jsonModel.setData([TEST_DATA.COMMON]);
        let answer = jsonModel.create(TEST_URL, {async: false, merge: true}, TEST_DATA.SEND);

        assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
        assert.equal(JSON.stringify(answer), JSON.stringify(expAnswer));
    });

    QUnit.test("create async, merge array", function(assert) {
        let expAnswer = {
            ... TEST_DATA.PROCESSED,
            ... TEST_DATA.COMMON
        }
        let expMergedData = [
            expAnswer,
            TEST_DATA.COMMON
        ]

        let finished = assert.async();
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData([TEST_DATA.COMMON]);
        jsonModel.create("", {
            async: true, 
            merge: true,
            success: function(responseData) {
                assert.equal(JSON.stringify(responseData), JSON.stringify(expAnswer));
                assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
                finished();
            },
            error: function() {
                assert.ok(false);
                finished();
            }
        }, TEST_DATA.COMMON);

        this.fakeServer.respond();
    });

    QUnit.test("call GET synch, merge true", function(assert) {
        let expMergedData = [
            TEST_URL_RESPONSE_JSON,
            TEST_DATA.COMMON
        ]

        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData([TEST_DATA.COMMON]);
        let answer = jsonModel.call("", "GET", {async: false, merge: true});
        

        assert.equal(JSON.stringify(answer), JSON.stringify(TEST_URL_RESPONSE_JSON));
        assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
    });

    QUnit.test("call GET async, merge true", function(assert) {
        let expMergedData = [
            TEST_URL_RESPONSE_JSON,
            TEST_DATA.COMMON
        ]

        let finished = assert.async();
        let jsonModel = new JSONModelExtended(ABSOLUTE_TEST_URL, true);
        jsonModel.setData([TEST_DATA.COMMON]);
        jsonModel.call("", "GET", {
            async: true, 
            merge: true,
            success: function(responseData) {
                assert.equal(JSON.stringify(responseData), JSON.stringify(TEST_URL_RESPONSE_JSON));
                assert.equal(jsonModel.getJSON(), JSON.stringify(expMergedData));
                finished();
            },
            error: function() {
                assert.ok(false);
                finished();
            }
        });

        this.fakeServer.respond();
    });

    QUnit.test("load data synch, merge false", function(assert) { 
        const newBaseURL = BASE_URL + TEST_URL;
        let jsonModel = new JSONModelExtended(BASE_URL, true);

        assert.equal(jsonModel.getBaseURL(), BASE_URL);

        jsonModel.setBaseURL(newBaseURL);

        assert.equal(jsonModel.getBaseURL(), newBaseURL);
    });
});
