# syntax=docker/dockerfile:1
FROM node:16.17.1-alpine3.15 as npm

COPY ./src/ /home/npm/ui5/de.jkeller.reuse/src
COPY ./package.json ./ui5.yaml /home/npm/ui5/de.jkeller.reuse/
WORKDIR /home/npm/ui5/de.jkeller.reuse
RUN mkdir -p /usr/local/lib/node_modules/de.jkeller.reuse
RUN npm install
RUN npm run build
RUN npm link
RUN ln -s /usr/local/lib/node_modules/de.jkeller.reuse