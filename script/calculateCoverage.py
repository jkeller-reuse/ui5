import xml.etree.ElementTree as ET
from lxml import etree as lxmlET
import sys
import math

def get_coverage_value(coverage_file):
    """
    :param coverage_file: location to the (Karma coverage) HTML file to gather the coverage information
    :return: the coverage of the project analyzed by (Karma coverage) HTML file. The returned map is
        containing the covered (key) and missed (key) statement information.
    :rtype: map
    """
    html_parsed = ET.parse(coverage_file, lxmlET.HTMLParser())
    if html_parsed is not None:
        root_node = html_parsed.getroot()
        if root_node is not None:
            all_files_overview = root_node.find("*//h1[.='All files']..")
            if all_files_overview is not None:
                statements_overview = all_files_overview.find("*//span[.='Statements']..")
                if statements_overview is not None:
                    fraction_span = statements_overview.find('./span[3]')
                    if fraction_span is not None:
                        covered, total = fraction_span.text.split("/")

    total = int(total) if total is not None and int(total) > 0 else 1
    covered = int(covered) if covered is not None else 0

    return {"covered": covered, "missed": total - covered}
            
def get_average_code_coverage(converage_files):
    """
    :param coverage_files: aray of location to (Karma coverage) HTML files to gather coverage information 
        and calculate fraction of sum of covered and missed statements.
    :return: the coverage gathered by all (Karma) coverage information, noramlized to [0, 100].
    :rtype: int
    """
    coverages = [get_coverage_value(coverage_file) for coverage_file in coverage_files]

    covered_sum = 0
    missed_sum = 0
    # sum(item['covered'] for item in coverages) # not used due to simultaneous sums 
    for coverage in coverages:
        covered_sum += coverage.get('covered')
        missed_sum += coverage.get('missed')
        
    overall_coverage = (covered_sum / (missed_sum + covered_sum)) * 100 if missed_sum > 0 else 0
    
    return round(overall_coverage, 2)

if __name__ == '__main__':
    """
    take a list of locations to (Karma coverage index) HTML files containing coverage information and print 
    the gathered coverage.
    """
    coverage_files = sys.argv[1:]
    
    print('overall code coverage: {:.2f}'.format(get_average_code_coverage(coverage_files)))

